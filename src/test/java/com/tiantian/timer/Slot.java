package com.tiantian.timer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 */
public class Slot <E> {
    private int index; // 第几个槽子
    private List<E> elements = new ArrayList<>();
    private final Lock lock = new ReentrantLock();

    public Slot(int index) {
        this.index = index;
    }

    public List<E> popAll() {
        lock.lock();
        try {
            if (elements.size() > 0) {
                List<E> allElements = new ArrayList<>(elements.size());
                for (int i = 0; i  < elements.size(); i++) {
                     allElements.add(elements.get(i));
                }
                elements.clear();
                return allElements;
            }
        } finally {
            lock.unlock();
        }
        return null;
    }

    public void pushElements(E e) {
        lock.lock();
        try {
            elements.add(e);
        } finally {
            lock.unlock();
        }
    }

    public int getIndex() {
        return index;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + index;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        @SuppressWarnings("rawtypes")
        Slot other = (Slot) obj;
        if (index != other.index)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Slot [index=" + index + ", elements=" + elements + "]";
    }
}
