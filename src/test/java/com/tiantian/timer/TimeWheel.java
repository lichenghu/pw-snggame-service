package com.tiantian.timer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 */
public class TimeWheel <E> {
    private List<Slot<E>> slotList;
    private int wheelTicks; // 一轮的tick数量即一轮slot的数量
    private long tickMills; // 每个tick的时间 (毫秒)
    private WheelListener<E> wheelListener;
    private int currentSlotIndex;
    private final ExecutorService service;

    public TimeWheel(long tickMills, int wheelTicks, WheelListener<E> wheelListener) {
           this(tickMills, wheelTicks, wheelListener, Runtime.getRuntime().availableProcessors());
    }

    public TimeWheel(long tickMills, int wheelTicks, WheelListener<E> wheelListener, int threadPoolSize) {
        this.tickMills = tickMills;
        this.wheelTicks = wheelTicks;
        slotList = new ArrayList<>(wheelTicks);
        for (int i = 0; i < wheelTicks; i++) {
             slotList.add(new Slot<>(i));
        }
        this.wheelListener = wheelListener;
        currentSlotIndex = (int) ((System.currentTimeMillis() / tickMills) % wheelTicks);
        service = Executors.newFixedThreadPool(Math.max(threadPoolSize, 1));
        start();
    }

    public void start() {
         new Thread(new TimeWorker()).start();
    }

    public void add(E e, long delayMill) {
        long current = System.currentTimeMillis();
        long time = current + Math.abs(delayMill);
        int index = (int) ((time / tickMills) % wheelTicks);
        slotList.get(index).pushElements(e);
    }

    private class TimeWorker implements Runnable {
        @Override
        public void run() {
            for (;;) {
                List<E> allElements = slotList.get(currentSlotIndex).popAll();
                if (allElements != null) {
                    if (wheelListener != null) {
                        service.execute(() -> wheelListener.wheelElements(allElements));
                    }
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(Math.max(1L, tickMills));
                    currentSlotIndex ++;
                    if (currentSlotIndex >= wheelTicks) {
                        currentSlotIndex = 0;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
