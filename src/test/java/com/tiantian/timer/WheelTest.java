package com.tiantian.timer;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 */
public class WheelTest {
    public static void main(String[] args) throws InterruptedException {
        TimeWheel<Task> taskTimeWheel = new TimeWheel<>(50L, 144000, new Listener());
        TimeUnit.SECONDS.sleep(1);
        for(int j = 0; j < 600; j++) {
             new Thread(() -> {
                 for (int i = 0; i < 500; i++) {
                     taskTimeWheel.add(new Task(i + ""), i * 10);
                 }
             }).start();
        }
        try {
            TimeUnit.SECONDS.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main2(String[] args) throws InterruptedException {
        ScheduledThreadPoolExecutor spe = new ScheduledThreadPoolExecutor(1, new PriorityThreadFactory("EffectsSTPool",
                Thread.NORM_PRIORITY));
        for (int i = 0; i < 1000000; i++) {
            final int a = i;
            spe.scheduleWithFixedDelay(() -> {

            }, 0, 10, TimeUnit.SECONDS);
        }

        TimeUnit.SECONDS.sleep(1000);
    }

    private static class PriorityThreadFactory implements ThreadFactory
    {
        private int _prio;
        private String _name;
        private AtomicInteger _threadNumber = new AtomicInteger(1);
        private ThreadGroup _group;

        public PriorityThreadFactory(String name, int prio)
        {
            _prio = prio;
            _name = name;
            _group = new ThreadGroup(_name);
        }

        @Override
        public Thread newThread(Runnable r)
        {
            Thread t = new Thread(_group, r);
            t.setName(_name + "-" + _threadNumber.getAndIncrement());
            t.setPriority(_prio);
            return t;
        }

        public ThreadGroup getGroup()
        {
            return _group;
        }
    }

    public static class Task {
        String name;

        public Task(String name) {
            this.name = name;
        }

        public String toString() {
            return name;
        }
    }

    public static class Listener implements WheelListener<Task> {
        @Override
        public void wheelElements(List<Task> elements) {
            for (Task task : elements) {
                 System.out.println(task.name);
            }
        }
    }
}
