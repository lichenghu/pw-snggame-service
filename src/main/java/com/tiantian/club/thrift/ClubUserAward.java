/**
 * Autogenerated by Thrift Compiler (0.9.3)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.tiantian.club.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.server.AbstractNonblockingServer.*;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import javax.annotation.Generated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@SuppressWarnings({"cast", "rawtypes", "serial", "unchecked"})
@Generated(value = "Autogenerated by Thrift Compiler (0.9.3)", date = "2016-10-26")
public class ClubUserAward implements org.apache.thrift.TBase<ClubUserAward, ClubUserAward._Fields>, java.io.Serializable, Cloneable, Comparable<ClubUserAward> {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ClubUserAward");

  private static final org.apache.thrift.protocol.TField CUA_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("cuaId", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField AWARD_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("awardId", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField AWARD_NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("awardName", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField CLUB_NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("clubName", org.apache.thrift.protocol.TType.STRING, (short)4);
  private static final org.apache.thrift.protocol.TField STATUS_FIELD_DESC = new org.apache.thrift.protocol.TField("status", org.apache.thrift.protocol.TType.I32, (short)5);
  private static final org.apache.thrift.protocol.TField CREATE_TIME_FIELD_DESC = new org.apache.thrift.protocol.TField("createTime", org.apache.thrift.protocol.TType.I64, (short)6);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ClubUserAwardStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ClubUserAwardTupleSchemeFactory());
  }

  public String cuaId; // required
  public String awardId; // required
  public String awardName; // required
  public String clubName; // required
  public int status; // required
  public long createTime; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    CUA_ID((short)1, "cuaId"),
    AWARD_ID((short)2, "awardId"),
    AWARD_NAME((short)3, "awardName"),
    CLUB_NAME((short)4, "clubName"),
    STATUS((short)5, "status"),
    CREATE_TIME((short)6, "createTime");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // CUA_ID
          return CUA_ID;
        case 2: // AWARD_ID
          return AWARD_ID;
        case 3: // AWARD_NAME
          return AWARD_NAME;
        case 4: // CLUB_NAME
          return CLUB_NAME;
        case 5: // STATUS
          return STATUS;
        case 6: // CREATE_TIME
          return CREATE_TIME;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __STATUS_ISSET_ID = 0;
  private static final int __CREATETIME_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.CUA_ID, new org.apache.thrift.meta_data.FieldMetaData("cuaId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.AWARD_ID, new org.apache.thrift.meta_data.FieldMetaData("awardId", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.AWARD_NAME, new org.apache.thrift.meta_data.FieldMetaData("awardName", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.CLUB_NAME, new org.apache.thrift.meta_data.FieldMetaData("clubName", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.STATUS, new org.apache.thrift.meta_data.FieldMetaData("status", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.CREATE_TIME, new org.apache.thrift.meta_data.FieldMetaData("createTime", org.apache.thrift.TFieldRequirementType.DEFAULT, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I64)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ClubUserAward.class, metaDataMap);
  }

  public ClubUserAward() {
  }

  public ClubUserAward(
    String cuaId,
    String awardId,
    String awardName,
    String clubName,
    int status,
    long createTime)
  {
    this();
    this.cuaId = cuaId;
    this.awardId = awardId;
    this.awardName = awardName;
    this.clubName = clubName;
    this.status = status;
    setStatusIsSet(true);
    this.createTime = createTime;
    setCreateTimeIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ClubUserAward(ClubUserAward other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetCuaId()) {
      this.cuaId = other.cuaId;
    }
    if (other.isSetAwardId()) {
      this.awardId = other.awardId;
    }
    if (other.isSetAwardName()) {
      this.awardName = other.awardName;
    }
    if (other.isSetClubName()) {
      this.clubName = other.clubName;
    }
    this.status = other.status;
    this.createTime = other.createTime;
  }

  public ClubUserAward deepCopy() {
    return new ClubUserAward(this);
  }

  @Override
  public void clear() {
    this.cuaId = null;
    this.awardId = null;
    this.awardName = null;
    this.clubName = null;
    setStatusIsSet(false);
    this.status = 0;
    setCreateTimeIsSet(false);
    this.createTime = 0;
  }

  public String getCuaId() {
    return this.cuaId;
  }

  public ClubUserAward setCuaId(String cuaId) {
    this.cuaId = cuaId;
    return this;
  }

  public void unsetCuaId() {
    this.cuaId = null;
  }

  /** Returns true if field cuaId is set (has been assigned a value) and false otherwise */
  public boolean isSetCuaId() {
    return this.cuaId != null;
  }

  public void setCuaIdIsSet(boolean value) {
    if (!value) {
      this.cuaId = null;
    }
  }

  public String getAwardId() {
    return this.awardId;
  }

  public ClubUserAward setAwardId(String awardId) {
    this.awardId = awardId;
    return this;
  }

  public void unsetAwardId() {
    this.awardId = null;
  }

  /** Returns true if field awardId is set (has been assigned a value) and false otherwise */
  public boolean isSetAwardId() {
    return this.awardId != null;
  }

  public void setAwardIdIsSet(boolean value) {
    if (!value) {
      this.awardId = null;
    }
  }

  public String getAwardName() {
    return this.awardName;
  }

  public ClubUserAward setAwardName(String awardName) {
    this.awardName = awardName;
    return this;
  }

  public void unsetAwardName() {
    this.awardName = null;
  }

  /** Returns true if field awardName is set (has been assigned a value) and false otherwise */
  public boolean isSetAwardName() {
    return this.awardName != null;
  }

  public void setAwardNameIsSet(boolean value) {
    if (!value) {
      this.awardName = null;
    }
  }

  public String getClubName() {
    return this.clubName;
  }

  public ClubUserAward setClubName(String clubName) {
    this.clubName = clubName;
    return this;
  }

  public void unsetClubName() {
    this.clubName = null;
  }

  /** Returns true if field clubName is set (has been assigned a value) and false otherwise */
  public boolean isSetClubName() {
    return this.clubName != null;
  }

  public void setClubNameIsSet(boolean value) {
    if (!value) {
      this.clubName = null;
    }
  }

  public int getStatus() {
    return this.status;
  }

  public ClubUserAward setStatus(int status) {
    this.status = status;
    setStatusIsSet(true);
    return this;
  }

  public void unsetStatus() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __STATUS_ISSET_ID);
  }

  /** Returns true if field status is set (has been assigned a value) and false otherwise */
  public boolean isSetStatus() {
    return EncodingUtils.testBit(__isset_bitfield, __STATUS_ISSET_ID);
  }

  public void setStatusIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __STATUS_ISSET_ID, value);
  }

  public long getCreateTime() {
    return this.createTime;
  }

  public ClubUserAward setCreateTime(long createTime) {
    this.createTime = createTime;
    setCreateTimeIsSet(true);
    return this;
  }

  public void unsetCreateTime() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __CREATETIME_ISSET_ID);
  }

  /** Returns true if field createTime is set (has been assigned a value) and false otherwise */
  public boolean isSetCreateTime() {
    return EncodingUtils.testBit(__isset_bitfield, __CREATETIME_ISSET_ID);
  }

  public void setCreateTimeIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __CREATETIME_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case CUA_ID:
      if (value == null) {
        unsetCuaId();
      } else {
        setCuaId((String)value);
      }
      break;

    case AWARD_ID:
      if (value == null) {
        unsetAwardId();
      } else {
        setAwardId((String)value);
      }
      break;

    case AWARD_NAME:
      if (value == null) {
        unsetAwardName();
      } else {
        setAwardName((String)value);
      }
      break;

    case CLUB_NAME:
      if (value == null) {
        unsetClubName();
      } else {
        setClubName((String)value);
      }
      break;

    case STATUS:
      if (value == null) {
        unsetStatus();
      } else {
        setStatus((Integer)value);
      }
      break;

    case CREATE_TIME:
      if (value == null) {
        unsetCreateTime();
      } else {
        setCreateTime((Long)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case CUA_ID:
      return getCuaId();

    case AWARD_ID:
      return getAwardId();

    case AWARD_NAME:
      return getAwardName();

    case CLUB_NAME:
      return getClubName();

    case STATUS:
      return getStatus();

    case CREATE_TIME:
      return getCreateTime();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case CUA_ID:
      return isSetCuaId();
    case AWARD_ID:
      return isSetAwardId();
    case AWARD_NAME:
      return isSetAwardName();
    case CLUB_NAME:
      return isSetClubName();
    case STATUS:
      return isSetStatus();
    case CREATE_TIME:
      return isSetCreateTime();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ClubUserAward)
      return this.equals((ClubUserAward)that);
    return false;
  }

  public boolean equals(ClubUserAward that) {
    if (that == null)
      return false;

    boolean this_present_cuaId = true && this.isSetCuaId();
    boolean that_present_cuaId = true && that.isSetCuaId();
    if (this_present_cuaId || that_present_cuaId) {
      if (!(this_present_cuaId && that_present_cuaId))
        return false;
      if (!this.cuaId.equals(that.cuaId))
        return false;
    }

    boolean this_present_awardId = true && this.isSetAwardId();
    boolean that_present_awardId = true && that.isSetAwardId();
    if (this_present_awardId || that_present_awardId) {
      if (!(this_present_awardId && that_present_awardId))
        return false;
      if (!this.awardId.equals(that.awardId))
        return false;
    }

    boolean this_present_awardName = true && this.isSetAwardName();
    boolean that_present_awardName = true && that.isSetAwardName();
    if (this_present_awardName || that_present_awardName) {
      if (!(this_present_awardName && that_present_awardName))
        return false;
      if (!this.awardName.equals(that.awardName))
        return false;
    }

    boolean this_present_clubName = true && this.isSetClubName();
    boolean that_present_clubName = true && that.isSetClubName();
    if (this_present_clubName || that_present_clubName) {
      if (!(this_present_clubName && that_present_clubName))
        return false;
      if (!this.clubName.equals(that.clubName))
        return false;
    }

    boolean this_present_status = true;
    boolean that_present_status = true;
    if (this_present_status || that_present_status) {
      if (!(this_present_status && that_present_status))
        return false;
      if (this.status != that.status)
        return false;
    }

    boolean this_present_createTime = true;
    boolean that_present_createTime = true;
    if (this_present_createTime || that_present_createTime) {
      if (!(this_present_createTime && that_present_createTime))
        return false;
      if (this.createTime != that.createTime)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    List<Object> list = new ArrayList<Object>();

    boolean present_cuaId = true && (isSetCuaId());
    list.add(present_cuaId);
    if (present_cuaId)
      list.add(cuaId);

    boolean present_awardId = true && (isSetAwardId());
    list.add(present_awardId);
    if (present_awardId)
      list.add(awardId);

    boolean present_awardName = true && (isSetAwardName());
    list.add(present_awardName);
    if (present_awardName)
      list.add(awardName);

    boolean present_clubName = true && (isSetClubName());
    list.add(present_clubName);
    if (present_clubName)
      list.add(clubName);

    boolean present_status = true;
    list.add(present_status);
    if (present_status)
      list.add(status);

    boolean present_createTime = true;
    list.add(present_createTime);
    if (present_createTime)
      list.add(createTime);

    return list.hashCode();
  }

  @Override
  public int compareTo(ClubUserAward other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;

    lastComparison = Boolean.valueOf(isSetCuaId()).compareTo(other.isSetCuaId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCuaId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.cuaId, other.cuaId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetAwardId()).compareTo(other.isSetAwardId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAwardId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.awardId, other.awardId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetAwardName()).compareTo(other.isSetAwardName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAwardName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.awardName, other.awardName);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetClubName()).compareTo(other.isSetClubName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetClubName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.clubName, other.clubName);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetStatus()).compareTo(other.isSetStatus());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetStatus()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.status, other.status);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCreateTime()).compareTo(other.isSetCreateTime());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCreateTime()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.createTime, other.createTime);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ClubUserAward(");
    boolean first = true;

    sb.append("cuaId:");
    if (this.cuaId == null) {
      sb.append("null");
    } else {
      sb.append(this.cuaId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("awardId:");
    if (this.awardId == null) {
      sb.append("null");
    } else {
      sb.append(this.awardId);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("awardName:");
    if (this.awardName == null) {
      sb.append("null");
    } else {
      sb.append(this.awardName);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("clubName:");
    if (this.clubName == null) {
      sb.append("null");
    } else {
      sb.append(this.clubName);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("status:");
    sb.append(this.status);
    first = false;
    if (!first) sb.append(", ");
    sb.append("createTime:");
    sb.append(this.createTime);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ClubUserAwardStandardSchemeFactory implements SchemeFactory {
    public ClubUserAwardStandardScheme getScheme() {
      return new ClubUserAwardStandardScheme();
    }
  }

  private static class ClubUserAwardStandardScheme extends StandardScheme<ClubUserAward> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ClubUserAward struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // CUA_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.cuaId = iprot.readString();
              struct.setCuaIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // AWARD_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.awardId = iprot.readString();
              struct.setAwardIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // AWARD_NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.awardName = iprot.readString();
              struct.setAwardNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // CLUB_NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.clubName = iprot.readString();
              struct.setClubNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // STATUS
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.status = iprot.readI32();
              struct.setStatusIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 6: // CREATE_TIME
            if (schemeField.type == org.apache.thrift.protocol.TType.I64) {
              struct.createTime = iprot.readI64();
              struct.setCreateTimeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ClubUserAward struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.cuaId != null) {
        oprot.writeFieldBegin(CUA_ID_FIELD_DESC);
        oprot.writeString(struct.cuaId);
        oprot.writeFieldEnd();
      }
      if (struct.awardId != null) {
        oprot.writeFieldBegin(AWARD_ID_FIELD_DESC);
        oprot.writeString(struct.awardId);
        oprot.writeFieldEnd();
      }
      if (struct.awardName != null) {
        oprot.writeFieldBegin(AWARD_NAME_FIELD_DESC);
        oprot.writeString(struct.awardName);
        oprot.writeFieldEnd();
      }
      if (struct.clubName != null) {
        oprot.writeFieldBegin(CLUB_NAME_FIELD_DESC);
        oprot.writeString(struct.clubName);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(STATUS_FIELD_DESC);
      oprot.writeI32(struct.status);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(CREATE_TIME_FIELD_DESC);
      oprot.writeI64(struct.createTime);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ClubUserAwardTupleSchemeFactory implements SchemeFactory {
    public ClubUserAwardTupleScheme getScheme() {
      return new ClubUserAwardTupleScheme();
    }
  }

  private static class ClubUserAwardTupleScheme extends TupleScheme<ClubUserAward> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ClubUserAward struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetCuaId()) {
        optionals.set(0);
      }
      if (struct.isSetAwardId()) {
        optionals.set(1);
      }
      if (struct.isSetAwardName()) {
        optionals.set(2);
      }
      if (struct.isSetClubName()) {
        optionals.set(3);
      }
      if (struct.isSetStatus()) {
        optionals.set(4);
      }
      if (struct.isSetCreateTime()) {
        optionals.set(5);
      }
      oprot.writeBitSet(optionals, 6);
      if (struct.isSetCuaId()) {
        oprot.writeString(struct.cuaId);
      }
      if (struct.isSetAwardId()) {
        oprot.writeString(struct.awardId);
      }
      if (struct.isSetAwardName()) {
        oprot.writeString(struct.awardName);
      }
      if (struct.isSetClubName()) {
        oprot.writeString(struct.clubName);
      }
      if (struct.isSetStatus()) {
        oprot.writeI32(struct.status);
      }
      if (struct.isSetCreateTime()) {
        oprot.writeI64(struct.createTime);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ClubUserAward struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(6);
      if (incoming.get(0)) {
        struct.cuaId = iprot.readString();
        struct.setCuaIdIsSet(true);
      }
      if (incoming.get(1)) {
        struct.awardId = iprot.readString();
        struct.setAwardIdIsSet(true);
      }
      if (incoming.get(2)) {
        struct.awardName = iprot.readString();
        struct.setAwardNameIsSet(true);
      }
      if (incoming.get(3)) {
        struct.clubName = iprot.readString();
        struct.setClubNameIsSet(true);
      }
      if (incoming.get(4)) {
        struct.status = iprot.readI32();
        struct.setStatusIsSet(true);
      }
      if (incoming.get(5)) {
        struct.createTime = iprot.readI64();
        struct.setCreateTimeIsSet(true);
      }
    }
  }

}

