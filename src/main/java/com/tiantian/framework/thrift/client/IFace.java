package com.tiantian.framework.thrift.client;

import org.apache.thrift.TServiceClient;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public abstract class IFace<T> {
    private Logger LOG = LoggerFactory.getLogger(IFace.class);
    private static Map<Integer, String> EXCEPTION_MAP = new HashMap<Integer, String>(){{
        put(TTransportException.NOT_OPEN, "TTransportException.NOT_OPEN");
        put(TTransportException.END_OF_FILE, "TTransportException.END_OF_FILE");
        put(TTransportException.TIMED_OUT, "TTransportException.TIMED_OUT");
        put(TTransportException.UNKNOWN, "TTransportException.UNKNOWN");
    }};
    private static final int MAX_TIMES = 3;
    private T clientProxy;
    private ClientPool clientPool;

    public IFace() {
         clientPool = createPool();
         ClassLoader classLoader = faceClass().getClassLoader();
         clientProxy = (T) Proxy.newProxyInstance(classLoader, new Class[]{faceClass()},
                 (proxy, method, args) -> {
                     TProtocol protocol = clientPool.borrowObject();
                     try {
                         int exeCount = 0;
                         return methodInvoker(protocol, method, args, exeCount);
                     } finally {
                         clientPool.returnObject(protocol);
                     }
                 });
    }

    protected abstract TServiceClient createClient(TProtocol tProtocol);

    protected abstract ClientPool createPool();

    protected abstract Class<T> faceClass();

    private Object methodInvoker(TProtocol tProtocol, Method method, Object[] args, int exeCount) throws Throwable {
        exeCount ++;
        TServiceClient client = createClient(tProtocol);
        //设置成可以访问
        method.setAccessible(true);
        try {
            return method.invoke(client, args);
        }
        catch (Throwable e) {
            if (e instanceof InvocationTargetException) {
                Throwable throwable = ((InvocationTargetException) e).getTargetException();
                if (throwable instanceof TTransportException || throwable instanceof IOException) {
                    if (throwable instanceof TTransportException) {
                        LOG.error("thrift error , type:" +
                                EXCEPTION_MAP.get(((TTransportException) throwable).getType()));
                    }
                    if (throwable instanceof IOException) {
                        LOG.error("thrift  IOException");
                    }
                    reconnectOrThrowException(tProtocol);
                    if (exeCount <= MAX_TIMES) { //尝试三次
                        LOG.error("thrift try times:" + exeCount);
                        return methodInvoker(tProtocol, method, args, exeCount);
                    }
                }
                throwable.printStackTrace();
                throw throwable;
            }
            e.printStackTrace();
            throw e;
        }
    }

    public T iface() {
        return clientProxy;
    }

    private void reconnectOrThrowException(TProtocol tProtocol) {
        TTransport transport = tProtocol.getTransport();
        LOG.info("thrift client reconnect");
        transport.close();
        try {
            transport.open();
        }
        catch (TTransportException e) {
            e.printStackTrace();
        }
    }

}
