package com.tiantian.snggame.akka.event;

import java.io.Serializable;

/**
 *
 */
public interface Game extends Serializable{
    String tableId();
}
