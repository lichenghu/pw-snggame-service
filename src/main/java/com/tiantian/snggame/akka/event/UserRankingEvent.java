package com.tiantian.snggame.akka.event;

import com.tiantian.sng.akka.event.RoomEvent;

import java.util.List;
import java.util.Map;

/**
 *
 */
public class UserRankingEvent implements RoomEvent {
    private String roomId;
    private String roomName;
    private String tableId;
    private Map<String, Integer> userRanking;
    private Map<String, Long> userReward;
    private Map<String, Map<String, String>> rewardMap;
    private List<String> overUserList; // 结束

    public UserRankingEvent(String roomId, String roomName, String tableId, Map<String, Integer> userRanking,
                            Map<String, Long> userReward, Map<String, Map<String, String>> rewardMap,  List<String> overUserList) {
        this.roomId = roomId;
        this.roomName = roomName;
        this.tableId = tableId;
        this.userRanking = userRanking;
        this.userReward = userReward;
        this.rewardMap = rewardMap;
        this.overUserList = overUserList;
    }

    @Override
    public String roomId() {
        return roomId;
    }

    @Override
    public String event() {
        return "userRanking";
    }

    public String getTableId() {
        return tableId;
    }

    public Map<String, Integer> getUserRanking() {
        return userRanking;
    }

    public Map<String, Long> getUserReward() {
        return userReward;
    }

    public List<String> getOverUserList() {
        return overUserList;
    }

    public void setOverUserList(List<String> overUserList) {
        this.overUserList = overUserList;
    }

    public Map<String, Map<String, String>> getRewardMap() {
        return rewardMap;
    }

    public String getRoomName() {
        return roomName;
    }
}
