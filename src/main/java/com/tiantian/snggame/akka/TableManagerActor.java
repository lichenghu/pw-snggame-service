package com.tiantian.snggame.akka;

import akka.actor.*;
import com.tiantian.sng.akka.event.Event;
import com.tiantian.sng.akka.event.TableEvent;
import com.tiantian.snggame.akka.actor.TableActor;
import com.tiantian.snggame.akka.event.GameOver;
import org.apache.commons.lang.StringUtils;
import scala.concurrent.duration.Duration;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class TableManagerActor extends UntypedActor {
    private Map<String, ActorRef> tableActorMap = new ConcurrentHashMap<>();

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
            if (message instanceof TableEvent) {
                String tableId = ((TableEvent) message).tableId();
                ActorRef tableActor = tableActorMap.get(tableId);
                if (tableActor == null) {
                    tableActor = getContext().actorOf(Props.create(TableActor.class, tableId)
                            .withDispatcher("dbDispatcher"), "Table_" + tableId);
                    tableActorMap.put(tableId, tableActor);
                }
                tableActor.forward(message, getContext());
            }
        }
        else if (message instanceof GameOver) { // 关闭桌子
            String tableId = ((GameOver) message).tableId();
            if (StringUtils.isNotBlank(tableId)) {
                ActorRef tableActor = tableActorMap.remove(tableId);
                if (tableActor != null) {
                    tableActor.tell(PoisonPill.getInstance(), ActorRef.noSender());
                }
            }
        }
        else {
            unhandled(message);
        }
    }

    private static SupervisorStrategy strategy =
            new OneForOneStrategy(
                    1, Duration.create("1 minute"),
                    t -> {
                        return SupervisorStrategy.resume();
                    });

    @Override
    public SupervisorStrategy supervisorStrategy() {
        return strategy;
    }

}
