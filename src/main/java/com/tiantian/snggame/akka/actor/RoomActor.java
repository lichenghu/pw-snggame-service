package com.tiantian.snggame.akka.actor;

import akka.actor.ActorRef;
import akka.actor.PoisonPill;
import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;
import com.tiantian.club.proxy_client.ClubIface;
import com.tiantian.mail.proxy_client.MailIface;
import com.tiantian.sng.akka.event.RoomEvent;
import com.tiantian.sng.akka.event.task.RoomTableOverEvent;
import com.tiantian.sng.akka.event.task.RoomTableStartEvent;
import com.tiantian.sng.akka.event.user.RoomUserExitEvent;
import com.tiantian.sng.akka.event.user.RoomUserExitUpdateStatusEvent;
import com.tiantian.sng.akka.event.user.RoomUserJoinEvent;
import com.tiantian.snggame.akka.event.UserRankingEvent;
import com.tiantian.snggame.data.mongodb.MGDatabase;
import com.tiantian.snggame.model.SngTable;
import com.tiantian.snggame.model.SngTableUser;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;
import org.bson.Document;
import org.bson.types.ObjectId;
import java.util.*;

/**
 *
 */
public class RoomActor extends UntypedActor {
    private final static String SNG_TABLES = "sng_tables";
    private final static String SNG_ROOMS = "sng_rooms";
    private String roomId;

    public RoomActor(String roomId) {
        this.roomId = roomId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof RoomEvent) {
            if (message instanceof RoomUserJoinEvent) {
                joinRoom(((RoomUserJoinEvent) message).getUserId(),
                        ((RoomUserJoinEvent) message).getClubId(),
                        ((RoomUserJoinEvent) message).roomId(),
                        ((RoomUserJoinEvent) message).getMaxTableNums(),
                        ((RoomUserJoinEvent) message).getMaxUserNums(),
                        ((RoomUserJoinEvent) message).getFee(),
                        ((RoomUserJoinEvent) message).getAvatarUrl(),
                        ((RoomUserJoinEvent) message).getUserName());
            } else if (message instanceof RoomUserExitEvent) {
                exitRoom(((RoomUserExitEvent) message).getTableId(),
                        ((RoomUserExitEvent) message).getUserId());
            } else if (message instanceof UserRankingEvent){
                System.out.println("UserRankingEvent:" + JSON.toJSONString((UserRankingEvent)message));
                updateRanking(((UserRankingEvent) message).getTableId(),
                        ((UserRankingEvent) message).getUserRanking(),
                        ((UserRankingEvent) message).getUserReward(),
                        ((UserRankingEvent) message).getRewardMap(),
                        ((UserRankingEvent) message).getOverUserList(),
                        ((UserRankingEvent) message).getRoomName());
            } else if (message instanceof RoomTableOverEvent) {
                roomTableOver(((RoomTableOverEvent) message).getTableId());
                checkGameOver(((RoomTableOverEvent) message).roomId());
            } else if (message instanceof RoomTableStartEvent) {
                roomTableStart(((RoomTableStartEvent) message).getTableId());
            } else if (message instanceof RoomUserExitUpdateStatusEvent) {
                updateUserStatusOver(((RoomUserExitUpdateStatusEvent) message).getTableId(),
                        ((RoomUserExitUpdateStatusEvent) message).getUserId());
            }
        }
        else {
            unhandled(message);
        }
    }

    private void joinRoom(String userId, String clubId, String roomId, int maxTableNums, int maxUserNums, long fee,
                          String avatarUrl, String userName) {
         List<SngTable> sngTables = getSngTable(roomId);
         if (sngTables == null || sngTables.size() == 0) {
             boolean ret = false;
             RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
             try {
                 ret = ClubIface.instance().iface().reduceUserClubScore(userId, clubId, fee);
             } catch (TException e) {
                 e.printStackTrace();
             }
             if (!ret) {
                 response.setStatus(-2); // 积分不足
                 getSender().tell(response, ActorRef.noSender());
                 return;
             }
             SngTable sngTable = createAndJoinSngTable(clubId, roomId, userId, maxUserNums, 1, avatarUrl, userName);

             response.setStatus(0);
             response.setTableId(sngTable.getTableId());
             response.setSitNum(sngTable.getTableUsers().get(0).getSitNums());
             response.setTableIndex(1);
             getSender().tell(response, ActorRef.noSender());
             return;
         }
        for (SngTable sngTable : sngTables) {
            if (sngTable.getIsEnd() == 1) { // 已经结束
                continue;
            }
            for (SngTableUser sngTableUser : sngTable.getTableUsers()) {
                 String tableUserId = sngTableUser.getUserId();
                 int ranking = sngTableUser.getRanking();
                 int sit = sngTableUser.getSitNums();
                 int tableIndex = sngTable.getTableIndex();
                 if (tableUserId.equalsIgnoreCase(userId) &&
                         ("normal".equalsIgnoreCase(sngTableUser.getStatus())
                                 || "exit".equalsIgnoreCase(sngTableUser.getStatus()))) {
                     RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
                     response.setStatus(0);
                     response.setTableId(sngTable.getTableId());
                     response.setSitNum(sit);
                     response.setTableIndex(tableIndex);
                     getSender().tell(response, ActorRef.noSender());
                     return;
                 }
            }
         }
         int sitNum = 0;
         String tableId = null;
         int tableIndex = 0;
         for (SngTable sngTable : sngTables) {
              if (sngTable.getMaxNumber() <= sngTable.getTableUsers().size()) { // 人数已经满
                  continue;
              }
              List<Integer> allSits = Lists.newArrayList();
              for (int i = 1; i <= maxUserNums; i++) {
                   allSits.add(i);
              }
              for (SngTableUser sngTableUser : sngTable.getTableUsers()) {
                   allSits.remove(Integer.valueOf( sngTableUser.getSitNums()));
              }
              int sitIndex = new Random().nextInt(allSits.size());
              sitNum = allSits.get(sitIndex);
              tableId = sngTable.getTableId();
              tableIndex = sngTable.getTableIndex();
              boolean ret = false;
              RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
              try {
                  ret = ClubIface.instance().iface().reduceUserClubScore(userId, clubId, fee);
              } catch (TException e) {
                  e.printStackTrace();
              }
              if (!ret) {
                  response.setStatus(-2); // 积分不足
                  getSender().tell(response, ActorRef.noSender());
                  return;
              }
              joinTable(tableId, userId, sitNum, avatarUrl, userName);
              break;
         }
         if (sitNum != 0) {
             RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
             response.setStatus(0);
             response.setSitNum(sitNum);
             response.setTableId(tableId);
             response.setTableIndex(tableIndex);
             getSender().tell(response, ActorRef.noSender());
             return;
         }

         if (sngTables.size() >= maxTableNums && maxTableNums > 0) { // 已经达到最大数
            // 局已经满了
            RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
            response.setStatus(-1);
            getSender().tell(response, ActorRef.noSender());
            return;
         }
         boolean ret = false;
         RoomUserJoinEvent.Response response = new RoomUserJoinEvent.Response();
         try {
             ret = ClubIface.instance().iface().reduceUserClubScore(userId, clubId, fee);
         } catch (TException e) {
             e.printStackTrace();
         }
         if (!ret) {
             response.setStatus(-2); // 积分不足
             getSender().tell(response, ActorRef.noSender());
             return;
         }
         SngTable sngTable = createAndJoinSngTable(clubId, roomId, userId, maxUserNums, sngTables.size() + 1, avatarUrl, userName);
         response.setStatus(0);
         response.setTableId(sngTable.getTableId());
         response.setSitNum(sngTable.getTableUsers().get(0).getSitNums());
         response.setTableIndex(sngTables.size() + 1);
         getSender().tell(response, ActorRef.noSender());
    }

    private void exitRoom(String tableId, String userId) {
       SngTable sngTable = getSngTableById(tableId);
       if (sngTable == null) {
           getSender().tell("0", ActorRef.noSender());
           return;
       }
       if (sngTable.getIsEnd() == 1) {
           getSender().tell("0", ActorRef.noSender());
           return;
       }
       List<SngTableUser> sngTableUsers = sngTable.getTableUsers();
       boolean hasFind = false;
       if (sngTableUsers != null && sngTableUsers.size() > 0) {
           Iterator<SngTableUser> iterator = sngTableUsers.iterator();
           while (iterator.hasNext()) {
                  SngTableUser sngTableUser = iterator.next();
                  if (sngTableUser.getRanking() == 0 && sngTableUser.getUserId().equals(userId)) {
                      iterator.remove();
                      hasFind = true;
                      break;
                  }
           }
       }
       if (hasFind) {
           updateTable(tableId, sngTableUsers);
           getSender().tell("1", ActorRef.noSender());
       }
       getSender().tell("0", ActorRef.noSender());
    }

    public void updateRanking(String tableId, Map<String, Integer> userRanking,
                              Map<String, Long> userScore, Map<String, Map<String, String>> rewardMap,
                              List<String> overList, String roomName) {
        System.out.println("userRanking:" + JSON.toJSONString(userRanking));
        System.out.println("userScore:" + JSON.toJSONString(userScore));
        System.out.println("rewardMap:" + JSON.toJSONString(rewardMap));
        System.out.println("overList:" + JSON.toJSONString(rewardMap));
        if (overList == null) {
            overList = Lists.newArrayList();
        }
        SngTable sngTable = getSngTableById(tableId);
        Map<String ,String> mailResult = Maps.newHashMap();
        if (sngTable != null &&  sngTable.getTableUsers() != null && sngTable.getTableUsers().size() > 0) {
            for (SngTableUser sngTableUser : sngTable.getTableUsers()) {
                 String uId = sngTableUser.getUserId();
                 Integer ranking = userRanking.get(uId);
                 if (ranking != null) {
                     sngTableUser.setRanking(ranking);
                     if (overList.contains(uId)) {
                         sngTableUser.setStatus("over");
                         mailResult.put(uId, "你在SNG比赛中获得第" + ranking + "名");
                     }
                 }
            }
            updateTable(tableId, sngTable.getTableUsers());
            if (userScore != null && userScore.size() > 0) {
                for (Map.Entry<String, Long> reward : userScore.entrySet()) {
                    String userId = reward.getKey();
                    Long score = reward.getValue();
                    try {
                        if (score > 0) {
                            ClubIface.instance().iface().addUserClubScore(userId, sngTable.getClubId(), score);
                        }
                        String mailContent = mailResult.get(userId);
                        if (StringUtils.isNotBlank(mailContent)) {
                            if (score > 0) {
                                mailContent += ",赢取" + score + "积分";
                                mailResult.put(userId, mailContent);
                            }
                        }
                    } catch (TException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (rewardMap != null && rewardMap.size() > 0) {
                for (Map.Entry<String, Map<String, String>> entry : rewardMap.entrySet()) {
                     String userId = entry.getKey();
                     Map<String, String> userReward = entry.getValue();
                     if (userReward != null) {
                        String rewardId = userReward.get("rewardId");
                        String nickName = "";
                        List<SngTableUser> sngTableUsers = sngTable.getTableUsers();
                        for (SngTableUser sngTableUser : sngTableUsers) {
                            if (userId.equals(sngTableUser.getUserId())) {
                                nickName = sngTableUser.getNickName();
                            }
                        }
                        Integer ranking = userRanking.get(userId);
                         try {
                             ClubIface.instance().iface().addUserReward(userId, rewardId, nickName, sngTable.getClubId(),
                                     roomName, ranking == null ? 0 : ranking);
                         } catch (TException e) {
                             e.printStackTrace();
                         }
                         String mailContent = mailResult.get(userId);
                         if (StringUtils.isNotBlank(mailContent)) {
                             mailContent += ",奖品" + userReward.get("rewardName");
                             mailResult.put(userId, mailContent);
                         }
                     }
                }
            }


            if (mailResult.size() > 0) {
                for (Map.Entry<String, String> entry : mailResult.entrySet()) {
                    //  发送邮件
                    try {
                        MailIface.instance().iface().saveNotice(entry.getKey(), entry.getValue());
                    } catch (TException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void roomTableOver(String tableId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(tableId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isEnd", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void checkGameOver(String roomId) {
        int maxTables = getRoomMaxTables(roomId);
        if (maxTables <= 0) {
            return;
        }
        List<SngTable> allTables = getSngTable(roomId);
        int cnt = 0;
        if (allTables != null && allTables.size() > 0) {
            for (SngTable sngTable : allTables) {
                 if (sngTable.getIsEnd() == 1) {
                     cnt ++;
                 }
            }
        }
        if (cnt == maxTables) {
            roomOver(roomId);
            getSelf().tell(PoisonPill.getInstance(), ActorRef.noSender());// 自杀
        }
    }

    private void roomOver(String roomId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_ROOMS);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(roomId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isEnd", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private int getRoomMaxTables(String roomId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_ROOMS);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(roomId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               Integer max = doc.getInteger("maxTableNums");
               return max == null ? 0 : max;
        }
        return 0;
    }

    private void roomTableStart(String tableId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(tableId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("isStarted", 1);
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateUserStatusOver(String tableId, String userId) {
        MongoCollection<Document> groupsCollection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(tableId));
        updateCondition.put("tableUsers.userId", userId);
        updateCondition.put("tableUsers.status", new BasicDBObject("$ne", "over"));
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("tableUsers.$.status", "exit");
        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        groupsCollection.updateOne(updateCondition, updateSetValue);
    }

    private SngTable getSngTableById(String tableId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("_id", new ObjectId(tableId));
        FindIterable<Document> limitIter = collection.find(selectCondition);
        MongoCursor<Document> cursor = limitIter.iterator();
        while (cursor.hasNext()) {
              Document doc = cursor.next();
              return getSngTableFormDoc(doc);
        }
        return null;
    }

    private List<SngTable> getSngTable(String roomId) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject selectCondition = new BasicDBObject();
        selectCondition.put("roomId", roomId);
        FindIterable<Document> limitIter = collection.find(selectCondition)
                .sort(new BasicDBObject("createDate", 1));
        MongoCursor<Document> cursor = limitIter.iterator();
        List<SngTable> sngTables = Lists.newArrayList();
        while (cursor.hasNext()) {
               Document doc = cursor.next();
               SngTable sngTable = getSngTableFormDoc(doc);
               sngTables.add(sngTable);
        }
        return sngTables;
    }

    private SngTable createAndJoinSngTable(String clubId, String roomId, String userId, int maxNumber, int tableIndex,
                                           String avatarUrl, String nickName) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        Document document = new Document();
        document.put("roomId", roomId);
        document.put("clubId", clubId);
        document.put("maxNumber", maxNumber);
        document.put("isStarted", 0);
        document.put("isEnd", 0);
        long createDate = System.currentTimeMillis();
        document.put("createDate", createDate);
        document.put("tableIndex", tableIndex);
        List<Document> tableUsers = Lists.newArrayList();
        int sit = new Random().nextInt(maxNumber) + 1;
        Document tableUser = new Document();
        tableUser.put("sitNums", sit);
        tableUser.put("userId", userId);
        tableUser.put("ranking", 0);
        tableUser.put("status", "normal");
        tableUser.put("avatarUrl", StringUtils.isBlank(avatarUrl) ? "" : avatarUrl);
        tableUser.put("nickName", nickName);

        tableUsers.add(tableUser);
        document.put("tableUsers", tableUsers);
        collection.insertOne(document);

        SngTable sngTable = new SngTable();
        sngTable.setTableId(document.getObjectId("_id").toString());
        sngTable.setRoomId(roomId);
        sngTable.setMaxNumber(maxNumber);
        sngTable.setIsStarted(0);
        sngTable.setIsEnd(0);
        sngTable.setCreateDate(createDate);
        List<SngTableUser> list = Lists.newArrayList();
        SngTableUser sngTableUser = new SngTableUser();
        sngTableUser.setUserId(userId);
        sngTableUser.setSitNums(sit);
        list.add(sngTableUser);
        sngTable.setTableUsers(list);
        return sngTable;
    }

    private SngTable getSngTableFormDoc(Document doc) {
        ObjectId objectId = doc.getObjectId("_id");
        String $roomId = doc.getString("roomId");
        String clubId = doc.getString("clubId");
        Integer maxNumber = doc.getInteger("maxNumber");
        Integer isStarted = doc.getInteger("isStarted");
        Integer isEnd = doc.getInteger("isEnd");
        Long createDate = doc.getLong("createDate");
        Integer tableIndex = doc.getInteger("tableIndex");
        SngTable sngTable = new SngTable();
        sngTable.setTableId(objectId.toString());
        sngTable.setClubId(clubId);
        sngTable.setMaxNumber(maxNumber);
        sngTable.setIsStarted(isStarted);
        sngTable.setCreateDate(createDate);
        sngTable.setRoomId($roomId);
        sngTable.setIsEnd(isEnd);
        sngTable.setTableIndex(tableIndex);
        List<Document> tableUsers = doc.get("tableUsers", List.class);
        List<SngTableUser> sngTableUsers = Lists.newArrayList();
        if (tableUsers != null) {
            for (Document document : tableUsers) {
                String userId = document.getString("userId");
                Integer sitNums = document.getInteger("sitNums");
                Integer ranking = document.getInteger("ranking");
                String status = document.getString("status");
                String avatarUrl = document.getString("avatarUrl");
                String nickName = document.getString("nickName");
                SngTableUser sngTableUser = new SngTableUser();
                sngTableUser.setUserId(userId);
                sngTableUser.setSitNums(sitNums);
                sngTableUser.setRanking(ranking);
                sngTableUser.setStatus(status);
                sngTableUser.setNickName(nickName);
                sngTableUser.setAvatarUrl(avatarUrl);
                sngTableUsers.add(sngTableUser);
            }
        }
        sngTable.setTableUsers(sngTableUsers);
        return sngTable;
    }

    private void joinTable(String tableId, String userId, int sitNum, String avatarUrl, String nickName) {
        // 添加成员
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        FindIterable<Document> limitIter = collection.find(new BasicDBObject("_id", new ObjectId(tableId)));
        MongoCursor<Document> cursor = limitIter.iterator();
        Document doc = null;
        List<Document> tableUsers = null;
        while (cursor.hasNext()) {
            doc = cursor.next();
            tableUsers = doc.get("tableUsers", List.class);
            if (tableUsers == null) {
                tableUsers = new ArrayList<>();
            }
            Document memDocument = new Document();
            memDocument.put("sitNums", sitNum);
            memDocument.put("userId", userId);
            memDocument.put("ranking", 0);
            memDocument.put("status", "normal");
            memDocument.put("avatarUrl", StringUtils.isBlank(avatarUrl) ? "" : avatarUrl);
            memDocument.put("nickName", nickName);
            tableUsers.add(memDocument);
        }
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(tableId));

        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("tableUsers", tableUsers);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    private void updateTable(String tableId, List<SngTableUser> sngTableUsers) {
        MongoCollection<Document> collection = MGDatabase.getInstance().getDB().getCollection(SNG_TABLES);
        BasicDBObject updateCondition = new BasicDBObject();
        updateCondition.put("_id", new ObjectId(tableId));

        List<Document> tableUsers = new ArrayList<>();
        for (SngTableUser sngTableUser : sngTableUsers) {
             Document document = new Document();
             document.put("sitNums", sngTableUser.getSitNums());
             document.put("userId", sngTableUser.getUserId());
             document.put("ranking", sngTableUser.getRanking());
             document.put("status", sngTableUser.getStatus());
             document.put("avatarUrl", sngTableUser.getAvatarUrl());
             document.put("nickName", sngTableUser.getNickName());
             tableUsers.add(document);
        }
        BasicDBObject updatedValue = new BasicDBObject();
        updatedValue.put("tableUsers", tableUsers);

        BasicDBObject updateSetValue = new BasicDBObject("$set", updatedValue);
        collection.updateOne(updateCondition, updateSetValue);
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }
}
