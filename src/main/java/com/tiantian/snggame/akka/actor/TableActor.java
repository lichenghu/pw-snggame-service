package com.tiantian.snggame.akka.actor;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import com.alibaba.fastjson.JSON;
import com.tiantian.sng.akka.event.*;
import com.tiantian.snggame.handlers.Handlers;

/**
 *
 */
public class TableActor extends UntypedActor {
    private String tableId;
    public TableActor(String tableId) {
        this.tableId = tableId;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
            if(message instanceof TableUserEvent) {
               if (((TableUserEvent)message).isNeedRet()) {
                   Object retObj = Handlers.INSTANCE.executeRet((TableUserEvent) message);
                   getSender().tell(retObj, ActorRef.noSender());
               }
               else {
                   Handlers.INSTANCE.execute((TableUserEvent) message, getSelf(), getContext(), getSender());
               }
            }
            else if (message instanceof TableTaskEvent) {
                Handlers.INSTANCE.execute((TableTaskEvent) message, getSelf(), getContext(), getSender());
            }
        }
        else {
            unhandled(message);
        }
    }
//
//    private static SupervisorStrategy strategy =
//            new OneForOneStrategy(
//                    1, Duration.create("1 minute"),
//                    t -> {
//                         return SupervisorStrategy.resume();
//                    });
//
//    @Override
//    public SupervisorStrategy supervisorStrategy() {
//        return strategy;
//    }
}
