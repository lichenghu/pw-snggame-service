package com.tiantian.snggame.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.client.ClusterClientReceptionist;
import com.tiantian.sng.akka.event.RoomEvent;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 *
 */
public class ClusterActorManager {
    private static ActorSystem actorSystem;
    private static String SHARDING_ACTOR_NAME = "SngClusterActorSystem";
    private static ActorRef tableActorRef;
    private static ActorRef roomActorRef;
    public static void init(String port) {
        Config config = ConfigFactory.parseString(
                "akka.remote.netty.tcp.port=" + port).withFallback(
                ConfigFactory.load("clusterconfig.conf"));

        actorSystem = ActorSystem.create(SHARDING_ACTOR_NAME, config);
        tableActorRef = actorSystem.actorOf(Props.create(ShardingManager.class, port), "ShardingManager");
        ClusterClientReceptionist.get(actorSystem).registerService(tableActorRef);

        roomActorRef = actorSystem.actorOf(Props.create(RoomShardingManager.class, port), "RoomShardingManager");
        ClusterClientReceptionist.get(actorSystem).registerService(roomActorRef);
    }

    public static void tellRoom(RoomEvent roomEvent) {
        roomActorRef.tell(roomEvent, ActorRef.noSender());
    }
}
