package com.tiantian.snggame.akka;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import com.tiantian.sng.akka.event.Event;
import com.tiantian.sng.akka.event.RoomEvent;
import com.tiantian.snggame.akka.actor.RoomActor;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class RoomManagerActor extends UntypedActor {
    private Map<String, ActorRef> roomActorMap = new ConcurrentHashMap<>();

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof Event) {
            if (message instanceof RoomEvent) {
                String roomId = ((RoomEvent) message).roomId();
                ActorRef roomActor = roomActorMap.get(roomId);
                if (roomActor == null) {
                    roomActor = getContext().actorOf(Props.create(RoomActor.class, roomId)
                            .withDispatcher("dbDispatcher"), "Room_" + roomId);
                    roomActorMap.put(roomId, roomActor);
                }
                roomActor.forward(message, getContext());
            }
        }
        else {
            unhandled(message);
        }
    }
}
