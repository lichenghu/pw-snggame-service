package com.tiantian.snggame.server;

import com.tiantian.snggame.akka.ClusterActorManager;
import com.tiantian.snggame.data.mongodb.MGDatabase;
import com.tiantian.snggame.data.redis.RedisUtil;
import com.tiantian.snggame.manager.constants.GameConstants;

import java.util.Set;

/**
 *
 */
public class SnggameServer {
    public static void main(String[] args) {
//        String defaultPort = "2550";
//        if (args != null) {
//            defaultPort = args[0];
//        }
//        ClusterActorManager.init(defaultPort);

        checkRoomUserOnline();

        MGDatabase.getInstance().init();

        ClusterActorManager.init("3455");
        ClusterActorManager.init("3451");
    }

    private static void checkRoomUserOnline() {
        Set<String> keys = RedisUtil.getKeys(GameConstants.SPINGO_ROOM_ONLINE_USERS_KEY + "*");
        if (keys != null && keys.size() > 0) {
            for(String key : keys) {
                Set<String> userIds = RedisUtil.smembers(key);
                if (userIds != null && userIds.size() > 0) {
                    for (String userId : userIds) {
                        boolean isOnline = RedisUtil.exists(GameConstants.ROUTER_KEY + userId);
                        if (!isOnline) {
                            RedisUtil.srem(key, userId);
                        }
                    }
                }
            }
        }
    }
}
