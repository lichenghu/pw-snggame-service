package com.tiantian.snggame.manager.model;

import com.tiantian.snggame.data.redis.RedisUtil;
import com.tiantian.snggame.manager.constants.GameConstants;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TableAllUser {

    private String tableId;
    // 座位号和userId
    private Map<String, String> gamingSitUserMap;
    private Map<String, String> joinTableUserMap;
    private Map<String, String> gamingAndExitUserMap;
    // 桌子里面所有的玩家ID
    private Collection<String> onlineTableUserIds;


    public static TableAllUser load(String tableId) {
        TableAllUser tableAllUser = new TableAllUser();
        tableAllUser.tableId = tableId;
        Map<String, String> tableMap = RedisUtil.getMap(GameConstants.SPINGO_TABLE_USER_KEY + tableId);
        if (tableMap != null) {
            // 筛选出在gaming状态的游戏玩家
            Map<String, String> sitUserMap = new HashMap<>();
            Map<String, String> sitAndExitUserMap = new HashMap<>();
            Set<String> exitSet = new HashSet<>();
            for(Map.Entry<String, String> entry : tableMap.entrySet()) {
                String sitNum = entry.getKey();
                String userId = entry.getValue();
                TableUser tableUser = TableUser.load(userId, tableId);
                if (tableUser != null && !tableUser.isNull()
                                            && tableId.equalsIgnoreCase(tableUser.getTableId())) {
                    if ("gaming".equalsIgnoreCase(tableUser.getStatus())) {
                        sitUserMap.put(sitNum, userId);
                        sitAndExitUserMap.put(sitNum, userId);
                    }
                    if (tableUser.isExit()) { //退出游戏或临时推出后挂机
                        sitAndExitUserMap.put(sitNum, userId);
                        exitSet.add(sitNum);
                    }
                }
            }
            for (String exiSit : exitSet) { //删除挂机的
                 tableMap.remove(exiSit);
            }
            tableAllUser.gamingSitUserMap = sitUserMap;
            tableAllUser.joinTableUserMap = tableMap;
            tableAllUser.gamingAndExitUserMap = sitAndExitUserMap;

            Map<String, String> tableAllUserMap = RedisUtil.getMap(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId);
            if (tableAllUserMap != null) {
                tableAllUser.onlineTableUserIds = tableAllUserMap.keySet();
            }
        }
        return tableAllUser;
    }

    // 加入玩家
    public void joinUser(String userId, String sitNum) {
        RedisUtil.setMap(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum, userId);
        joinOnline(userId);
    }

    public void deleteUser(String sitNum) {
        if (StringUtils.isBlank(sitNum)) {
            return;
        }
        String userId = joinTableUserMap.get(sitNum);
        RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum);
        if (StringUtils.isNotBlank(userId)) {
            deleteOnline(userId);
        }
    }

    public void deleteUserByUserId(String userId) {
        if (joinTableUserMap != null) {
            for (Map.Entry<String, String> entry : joinTableUserMap.entrySet()) {
                 String sitNum = entry.getKey();
                 String uId = entry.getValue();
                 if (uId.equalsIgnoreCase(userId)) {
                     deleteUser(sitNum);
                     break;
                }
            }
        }
    }
    //删除掉已经离开的玩家
    public void flushGamingSitUser(List<String> removeSitList) {
        for (String sitNum : removeSitList) {
             RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_KEY + tableId, sitNum);
        }
    }

    // 根据玩家ID判断玩家是否在游戏中，并返回座位号，返回值为空则表示不在
    public String checkUserGaming(String userId) {
        if(gamingSitUserMap == null) {
           return null;
        }
        Set<Map.Entry<String, String>> entrySet = gamingSitUserMap.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
             String sitNum = entry.getKey();
             String uId = entry.getValue();
             if (uId.equalsIgnoreCase(userId)) {
                 return sitNum;
             }
        }
        return null;
    }

    public Collection<String> getOnlineTableUserIds() {
        return joinTableUserMap.values();
    }

    public void delSelf() {
        RedisUtil.del(GameConstants.SPINGO_TABLE_USER_KEY + tableId);
        RedisUtil.del(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId);
    }

    // 加入在线
    public void joinOnline(String userId) {
        RedisUtil.setMap(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId, userId, "");
    }

    // 删除在线
    public void deleteOnline(String userId) {
        RedisUtil.delMapField(GameConstants.SPINGO_TABLE_USER_ONLINE_KEY + tableId, userId);
    }

    public Map<String, String> getGamingSitUserMap() {
        return gamingSitUserMap;
    }

    public void setGamingSitUserMap(Map<String, String> gamingSitUserMap) {
        this.gamingSitUserMap = gamingSitUserMap;
    }

    public Map<String, String> getJoinTableUserMap() {
        return joinTableUserMap;
    }

    public void setJoinTableUserMap(Map<String, String> joinTableUserMap) {
        this.joinTableUserMap = joinTableUserMap;
    }

    public Map<String, String> getGamingAndExitUserMap() {
        return gamingAndExitUserMap;
    }

    public void setGamingAndExitUserMap(Map<String, String> gamingAndExitUserMap) {
        this.gamingAndExitUserMap = gamingAndExitUserMap;
    }

    public void setOnlineTableUserIds(Collection<String> onlineTableUserIds) {
        this.onlineTableUserIds = onlineTableUserIds;
    }
}
