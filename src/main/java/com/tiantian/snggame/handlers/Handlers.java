package com.tiantian.snggame.handlers;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.tiantian.sng.akka.event.Event;
import com.tiantian.snggame.handlers.task.*;
import com.tiantian.snggame.handlers.user.*;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameStatus;
import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
public class Handlers {

    public static Handlers INSTANCE = new Handlers();
    private static  Map<String, EventHandler> eventHandlers;
    private static Map<String, EventRetHandler> eventRetHandlers;

    private Handlers() {
        eventHandlers = new ConcurrentHashMap<>();
        eventRetHandlers = new ConcurrentHashMap<>();
        init();
    }

    private void registerHandler(String name, EventHandler handler) {
        eventHandlers.put(name, handler);
    }

    private void registerRetHandler(String name, EventRetHandler handler) {
        eventRetHandlers.put(name, handler);
    }

    private void init() {

        registerHandler(GameStatus.BEGIN.name(), new TableBeginHandler());
        registerHandler(GameStatus.D_AND_B.name(), new TableDAndBHandler());
        registerHandler(GameStatus.PRE_FLOP.name(), new TablePreFlopHandler());
        registerHandler(GameStatus.FLOP.name(), new TableFlopHandler());
        registerHandler(GameStatus.TURN.name(), new TableTurnHandler());
        registerHandler(GameStatus.RIVER.name(), new TableRiverHandler());
        registerHandler(GameStatus.FINISH.name(), new TableFinishHandler());

        registerHandler(GameConstants.TABLE_STATUS_CHECK, new TableCheckStatusHandler());
        registerHandler(GameConstants.CHECK_CHIPS, new TableCheckChipsHandler());
        registerHandler(GameConstants.TEST_STATUS, new TableTestStatusHandler());

        registerHandler(GameConstants.TEST_BET, new TableTestBetHandler());
        registerHandler(GameConstants.USER_RAISE, new UserRaiseHandler());
        registerHandler(GameConstants.USER_FAST_RAISE, new UserFastRaiseHandler());
        registerHandler(GameConstants.USER_ALLIN, new UserAllinHandler());
        registerHandler(GameConstants.USER_CALL, new UserCallHandler());
        registerHandler(GameConstants.USER_CHECK, new UserCheckHandler());
        registerHandler(GameConstants.USER_FOLD, new UserFoldHandler());

        registerHandler(GameConstants.USER_REJOIN, new UserRejoinHandler());
        registerHandler(GameConstants.EMOJI, new UserEmojiHandler());

//        registerHandler(GameConstants.TABLE_ONLINE_USER_CHECK, new );
        registerHandler(GameConstants.SHOW_CARDS, new UserShowCardsHandler());
        registerRetHandler(GameConstants.TABLE_INFO, new UserTableInfHandler());
        registerRetHandler(GameConstants.USER_JOIN, new UserTableJoinHandler());
        registerRetHandler(GameConstants.USER_EXIT, new UserExitHandler());
    }

    public void execute(Event tableEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String type = tableEvent.event();
        if (StringUtils.isBlank(type)) {
            throw new RuntimeException("event type can not be null");
        }
        EventHandler handler = eventHandlers.get(type);
        if (handler == null) {
            throw new RuntimeException("unregister event type :" + type);
        }
        try {
            handler.handler(tableEvent, self, context, sender);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public Object executeRet(Event tableEvent) throws Exception {
        String type = tableEvent.event();
        if (StringUtils.isBlank(type)) {
            throw new Exception("event type can not be null");
        }

        EventRetHandler handler = eventRetHandlers.get(type);
        if (handler == null) {
            throw new Exception("unregister event type :" + type);
        }
        try {
            return handler.handler(tableEvent);
        }
        catch (Throwable e) {
            e.printStackTrace();
        }
        return null;
    }
}
