package com.tiantian.snggame.handlers.helper;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.manager.texas.PokerManager;
import com.tiantian.snggame.manager.texas.PokerOuts;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class UserInfHandlerHelper {

    public static void sendUserInfos(TableStatus tableStatus, TableAllUser tableAllUser, String tableId, String userId,
                                 String nickName, String avatarUrl, String gender) {
        JSONObject dataObject = getTableInfos(tableStatus, tableAllUser, tableId, userId);
        dataObject.put("show_begin", 0);
        dataObject.put("is_stopped", 0);
        dataObject.put("game_status", StringUtils.isBlank(tableStatus.getStatus()) ? "" : tableStatus.getStatus().toLowerCase()) ;

       // checkUserStandUp(tableAllUser, userId, tableId, dataObject, nickName, avatarUrl, gender, tableStatus.getRoomId());
        // 发送其他玩家的消息
        GameUtils.notifyUser(dataObject, GameEventType.USERS_INFO, userId, tableStatus.getTableId());
    }

    private static JSONObject getTableInfos(TableStatus tableStatus, TableAllUser tableAllUser, String tableId,
                                     String userId) {
        JSONObject dataObject = new JSONObject();
        String innerId = tableStatus.getInningId() == null ? "" : tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount() == null ? "0" : tableStatus.getIndexCount();
        JSONArray array = new JSONArray();
        Map<String, String> gamingMap = null;
        if(tableStatus.isStarted()) {
            gamingMap = tableAllUser.getGamingAndExitUserMap();
        } else {
            gamingMap = tableAllUser.getJoinTableUserMap();
        }
        Set<Map.Entry<String, String>> entries = gamingMap.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            String $sitNum = entry.getKey();
            String $userId = entry.getValue();
            TableUser tableUser = TableUser.load($userId, tableId);
            if (tableUser.isNull()) {
                continue;
            }
            JSONObject $object = new JSONObject();
            $object.put("user_id", $userId);
            $object.put("room_id", tableStatus.getRoomId());
            $object.put("table_id", tableId);
            $object.put("sit_num", Integer.parseInt($sitNum));
            $object.put("avatar_url", tableUser.getAvatarUrl() == null ? "" : tableUser.getAvatarUrl());
            $object.put("nick_name", tableUser.getNickName());
            $object.put("gender", tableUser.getGender() == null ? "0" : tableUser.getGender());
            $object.put("is_master", 0);
            UserChips $userChips = UserChips.load($userId, tableId);
            long money = 0;
            if($userChips != null) {
                money = $userChips.getChips();
            }
            $object.put("money", money);
            // 玩家是否在游戏中 1是, 0否表示在等待
            $object.put("playing", gamingMap.containsKey($sitNum) ? 1 : 0);
            array.add($object);
        }
        dataObject.put("users", array);
        dataObject.put("room_id", tableStatus.getRoomId());
        dataObject.put("table_id", tableId);
        JSONObject $object = new JSONObject();
        List<Long> poolList = tableStatus.betPoolList();
        $object.put("pool", StringUtils.join(poolList, ","));
        // 牌局信息
        String deskCards = "";
        if (StringUtils.isNotBlank(tableStatus.getDeskCards())) {
            List<String> deskCardList = JSON.parseObject(tableStatus.getDeskCards(), List.class);
            deskCards = StringUtils.join(deskCardList, ",");
        }
        $object.put("desk_cards", deskCards);
        int curBetSit = -1;
        if (StringUtils.isNotBlank(tableStatus.getCurrentBet())) {
            curBetSit = Integer.valueOf(tableStatus.getCurrentBet());
        }
        $object.put("cur_bet_sit", curBetSit);
        String currentBetTimes = tableStatus.getCurrentBetTimes();
        long leftSecs = 0;
        if (StringUtils.isNotBlank(currentBetTimes)) {
            //判断当前玩家有没有弃牌,玩家有可能站起了
            String status = tableStatus.getSitBetStatusBySitNum(tableStatus.getCurrentBet());
            if (status != null && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                long times = System.currentTimeMillis() - Long.parseLong(currentBetTimes);
                leftSecs = Math.max(0, GameConstants.BET_DELAYER_TIME - times);
            }
        }
        $object.put("left_secs", leftSecs / 1000);
        String usersBetsLog = tableStatus.getUsersBetsLog();
        List<Map<String, Object>> userBetsList = new ArrayList<>();
        if (StringUtils.isNotBlank(usersBetsLog)) {
            List<UserBetLog> userBetLogs = JSON.parseArray(usersBetsLog, UserBetLog.class);
            for (UserBetLog userBetLog : userBetLogs) {
                if (!gamingMap.containsKey(userBetLog.getSitNum())) {
                    continue;
                }
                Map<String, Object> map = new HashMap<>();
                map.put("sn", Integer.valueOf(userBetLog.getSitNum()));
                map.put("bet", userBetLog.getRoundChips());
                userBetsList.add(map);
            }
        }
        $object.put("user_bets", userBetsList);
        List<Map<String, Object>> allSitBetStatusMap = tableStatus.allUserBetStatus();
        // 筛选出所有在游戏中的玩家状态
        List<Map<String, Object>> sitBetStatusMap = new ArrayList<>();
        for(Map<String, Object> map : allSitBetStatusMap) {
            Integer sit = (Integer) map.get("sn");
            if (gamingMap.containsKey(sit.toString())) {
                sitBetStatusMap.add(map);
            }
        }
        // 设置大盲注状态，开始时候默认设置了大盲注状态
        UserBetLog bigUserBetLog = tableStatus.getUserBetLog(tableStatus.getBigBlindNum());
        if(bigUserBetLog != null) {
            long chips = bigUserBetLog.getRoundChips() + bigUserBetLog.getTotalChips();
            if (chips != Long.parseLong(tableStatus.getBigBlindMoney())) { // 玩家没有下过注
                String bigStatus = tableStatus.getSitBetStatusBySitNum(tableStatus.getBigBlindNum());
                // 状态为初始状态
                if (GameEventType.CALL.equalsIgnoreCase(bigStatus)) {
                    for(Map<String, Object> map : sitBetStatusMap) {
                        if (map.containsKey(tableStatus.getBigBlindNum())) {
                            map.put(tableStatus.getBigBlindNum(), "");
                            break;
                        }
                    }
                }
            }
        }
        $object.put("user_status", sitBetStatusMap);
        int btn = -1;
        //TODO 根据房间开始类型判断是否显示庄
        if (StringUtils.isNotBlank(tableStatus.getButton()) && entries.size() > 2) {
            btn = Integer.parseInt(tableStatus.getButton());
        }
        $object.put("btn", btn); // 庄家位
        int smb = -1;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindNum())) {
            smb = Integer.parseInt(tableStatus.getSmallBlindNum());
        }
        $object.put("smb", smb); // 小盲注座位号
        int smbm = 0;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindMoney())) {
            smbm = Integer.parseInt(tableStatus.getSmallBlindMoney());
        }
        $object.put("smbm", smbm); //设置测试小盲注值
        int bgb = -1;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindNum())) {
            bgb = Integer.parseInt(tableStatus.getBigBlindNum());
        }
        $object.put("bgb", bgb); // 大盲注座位号
        int bgbm = 0;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindMoney())) {
            bgbm = Integer.parseInt(tableStatus.getBigBlindMoney());
        }
        $object.put("bgbm", bgbm); //设置测试大盲注
        String userCards = "";
        String cardLevel = "";
        Map<String, String> userCardsMap = tableStatus.getUsersCards();
        String userCanOps = "";
        String waitStatus = "";
        TableUser tableUser = TableUser.load(userId, tableId);
        String sitNum = "";
        if (userCardsMap != null) {
            if (tableUser != null) {
                sitNum = tableUser.getSitNum();
                if ("wait_blind".equalsIgnoreCase(tableUser.getStatus()) ||
                                        "bet_blind".equalsIgnoreCase(tableUser.getStatus())) {
                    waitStatus = tableUser.getStatus();
                }
                // 玩家必须还在游戏中
                if (StringUtils.isNotBlank(sitNum) && "gaming".equalsIgnoreCase(tableUser.getStatus())) {
                    userCards = userCardsMap.get(sitNum);
                    if (sitNum.equalsIgnoreCase(tableStatus.getCurrentBet())) {
                        String[] ops = tableStatus.getUserCanOps(tableUser.getUserId(), sitNum);
                        userCanOps = StringUtils.join(ops, ",");
                    }
                    PokerOuts pokerOuts = PokerManager.getPokerOuts(userCards, tableStatus.getDeskCardList());
                    if (pokerOuts != null) {
                        cardLevel = pokerOuts.getLevel() + "";
                    }
                }
            }
        }
        $object.put("hand_cards",  userCards == null ? "" : userCards);
        $object.put("card_level",  cardLevel);
        $object.put("user_can_ops", userCanOps);
        $object.put("wait_status",  waitStatus);
        $object.put("pwd", tableStatus.getPwd() == null || StringUtils.isBlank(userCanOps) ? "" : tableStatus.getPwd());
        dataObject.put("inner_id", innerId);
        dataObject.put("inner_cnt", indexCount);
        dataObject.put("desks", $object);
        dataObject.put("total_secs", tableUser == null || StringUtils.isBlank(tableUser.getTotalSecs()) ? 25 : Integer.parseInt(tableUser.getTotalSecs()));
        dataObject.put("wait_nums", Integer.parseInt(tableStatus.getMaxUsers()) - tableAllUser.getJoinTableUserMap().size());
        dataObject.put("ranking", tableUser == null || tableUser.getRanking() == null ? "0" : tableUser.getRanking());
        return dataObject;
    }

    private static void checkUserStandUp(TableAllUser tableAllUser, String userId, String groupId,
                                  JSONObject dataObject, String nickName, String avatarUrl, String gender, String roomId) {
        boolean isSit = tableAllUser.getJoinTableUserMap().values().contains(userId);
        JSONArray array = (JSONArray) dataObject.get("users");
        if (!isSit) {
            JSONObject $object = new JSONObject();
            $object.put("user_id", userId);
            $object.put("room_id", roomId);
            $object.put("table_id", groupId);
            $object.put("sit_num",  -1);
            $object.put("avatar_url", avatarUrl == null ? "" : avatarUrl);
            $object.put("gender", gender == null ? "0" : gender);
            $object.put("nick_name", nickName);
            $object.put("money", 0);
            $object.put("playing", 0);
            $object.put("is_master", 0);
            array.add($object);
        }
        dataObject.put("users", array);
    }
}
