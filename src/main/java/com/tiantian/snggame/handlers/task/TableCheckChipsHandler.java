package com.tiantian.snggame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.sng.akka.event.user.TableUserJoinEvent;
import com.tiantian.snggame.akka.ClusterActorManager;
import com.tiantian.snggame.akka.event.UserRankingEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import java.util.*;

/**
 *
 */
public class TableCheckChipsHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        String innerId = tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount();
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        String ruleStr = tableStatus.getBlindRules();
        List<TableUserJoinEvent.Rule> ruleList = JSON.parseArray(ruleStr, TableUserJoinEvent.Rule.class);
        int currentLvl = Integer.parseInt(tableStatus.getCurrentBlindLevel());
        TableUserJoinEvent.Rule rule = ruleList.get(currentLvl);
        long lastUpBlindTimes = Long.parseLong(tableStatus.getLastUpBlindTimes());
        long upBlindSec = rule.getUpBlindSecs();
        if ((System.currentTimeMillis() - lastUpBlindTimes) / 1000 >= upBlindSec) { // 需要升级盲注
            TableUserJoinEvent.Rule newRule = ruleList.get(currentLvl + 1);
            tableStatus.setSmallBlindMoney(newRule.getSmallBlind() + "");
            tableStatus.setBigBlindMoney(newRule.getBigBlind() + "");
            tableStatus.setCurrentBlindLevel((currentLvl + 1) + "");
            tableStatus.setLastUpBlindTimes(System.currentTimeMillis() + "");

            //发送升级盲注信息
            JSONObject object = new JSONObject();
            object.put("inner_id", innerId);
            object.put("inner_cnt", indexCount);
            object.put("small_blind", tableStatus.getSmallBlindMoney());
            object.put("big_blind", tableStatus.getBigBlindMoney());
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.BLIND_UP, tableAllUser.getOnlineTableUserIds(), id, tableId);
        }
        List<AllinInf> allinInfs = tableStatus.userAllInfos();
        tableStatus.clearNotFlush();
        tableStatus.saveNotAddCnt();


        Collection<String> userIds = tableAllUser.getGamingAndExitUserMap().values();

        List<String> overList = Lists.newArrayList();
        List<UserChips> userChipsList = Lists.newArrayList();
        for(String userId : userIds) {
            UserChips userChips = UserChips.load(userId, tableId);
            if (userChips == null || userChips.getChips() == 0){
                overList.add(userId);
            }
            userChipsList.add(userChips);
        }
        Collections.sort(userChipsList, (o1, o2) -> (int)(o2.getChips() - o1.getChips()));
        int tmpRanking = 1;
        Map<String, Integer> userRanking = Maps.newHashMap();
        long tmpMaxChips = userChipsList.get(0).getChips();
        for (UserChips userChips : userChipsList) {
             long userChip = userChips.getChips();
             if (tmpMaxChips  > userChip) {
                 tmpRanking ++;
             }
             else {
                 tmpMaxChips = userChip;
             }
             userRanking.put(userChips.getUserId(), tmpRanking);
        }
        //overList
        if (overList.size() > 0) {
            List<Integer> overRankings = Lists.newArrayList();
            for (String uId : overList) {
                 Integer uRanking = userRanking.get(uId);
                 if (uRanking != null) {
                     overRankings.add(uRanking);
                 }
            }
            Collections.sort(overRankings); // 升序排列
            Collections.sort(allinInfs, (o1, o2) -> {
                int a = (int)(o2.getAllChips() - o1.getAllChips());
                if (a != 0) {
                    return a;
                }
                return o1.getIndex() - o2.getIndex();
            });
            int index = 0;
            for (AllinInf allinInf : allinInfs) {
                 String allInUserId = allinInf.getUserId();
                 if (!overList.contains(allInUserId)) { // 必须是结束的
                     continue;
                 }
                 if (index >= overRankings.size()) {
                     continue;
                 }
                 userRanking.put(allInUserId, overRankings.get(index));
                 index++;
            }
        }

        String roomId = tableStatus.getRoomId();
        List<TableUserJoinEvent.Reward> rewards = null;
        String rewardStr = tableStatus.getRewards();
        if (StringUtils.isNotBlank(rewardStr)) {
            rewards = JSON.parseArray(rewardStr, TableUserJoinEvent.Reward.class);
        }
        // 排名的玩家
        Map<Integer, List<String>> rankingUserMap = Maps.newHashMap();
        for (Map.Entry<String, Integer> entry : userRanking.entrySet()) {
             String userId = entry.getKey();
             Integer ranking = entry.getValue();
             List<String> uIds = rankingUserMap.get(ranking);
             if (uIds == null) {
                 uIds = Lists.newArrayList();
             }
             uIds.add(userId);
             rankingUserMap.put(ranking, uIds);
        }

        Map<String, Map<String, String>> userRewardMap = Maps.newHashMap();
        Map<String, Long> userScoreMap = Maps.newHashMap();
        if (rewards != null) {
            for (TableUserJoinEvent.Reward reward : rewards) {
                 Integer ranking = reward.getRanking();
                 Long score = reward.getScore();
                 List<String> uIds = rankingUserMap.get(ranking);
                 if (uIds == null) {
                     continue;
                 }
                 long perScore = score / uIds.size();
                 long leftScore = score % uIds.size();
                 for (String uId : uIds) {
                      if (!overList.contains(uId)) { //玩家已经结束游戏
                          continue;
                      }
                      leftScore --;
                      long userScore = perScore;
                      if (leftScore >= 0) {
                          userScore ++;
                      }
                      userScoreMap.put(uId, userScore);
                 }
                 if (StringUtils.isNotBlank(reward.getAwardId())) {
                     if (overList.contains(uIds.get(0))) { // 游戏未结束时必须是已经结束才能发放奖品
                         Map<String, String> rewardMap = Maps.newHashMap();
                         rewardMap.put("rewardId", reward.getAwardId());
                         rewardMap.put("rewardName", reward.getAwardName());
                         userRewardMap.put(uIds.get(0), rewardMap);
                     }
                 }
            }
        }
        // 修改所有人的排名
        ClusterActorManager.tellRoom(new UserRankingEvent(roomId, tableStatus.getRoomName(), tableStatus.getTableId(), userRanking,
                userScoreMap, userRewardMap, overList));

        for (String userId : userIds) {
            TableUser tableUser = TableUser.load(userId, tableId);
            if (tableUser == null || tableUser.isNull()) {
                continue;
            }
            Integer ranking = userRanking.get(userId);
            String userTableId = tableUser.getTableId();
            if (tableId.equalsIgnoreCase(userTableId)) {
                tableUser.setOperateCount(null);
                tableUser.setBetStatus("");
                tableUser.setShowCards("");
                // 设置排名
                tableUser.setRanking(ranking + "");
            }
            //判断筹码
            checkUserChips(userId, tableId,
                    tableUser.getSitNum(), tableAllUser.getOnlineTableUserIds(), tableUser,
                    innerId, indexCount, ranking, tableAllUser);
            tableUser.save();
        }

        JSONObject data = new JSONObject();
        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        data.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        data.put("tableId", tableId);

        Handlers.INSTANCE.execute(new TableTaskEvent(GameStatus.BEGIN.name(), data), self, context, sender);
    }

    private void checkUserChips(String userId, String tableId,
                                String sitNum, Collection<String> toUserIds, TableUser tableUser,
                                String innerId, String innerCnt, int ranking, TableAllUser tableAllUser) {
        UserChips userChips = UserChips.load(userId, tableId);
        boolean isOver = false;
        if (userChips != null && userChips.getChips() == 0) {
            isOver = true;
            userChips.delSelf(tableId); //删除筹码
            if(tableUser == null || tableUser.isNull()
                    || "standing".equalsIgnoreCase(tableUser.getStatus())) {
               return;
            }
            // 站起
            tableUser.forceStandUp();

            // 通知玩家离开
            JSONObject object1 = new JSONObject();
            object1.put("inner_id", innerId);
            object1.put("inner_cnt", innerCnt);
            object1.put("uid", userId);
            object1.put("sn", Integer.parseInt(sitNum));
            object1.put("reason", "");
            String id = UUID.randomUUID().toString().replace("-", "");
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUsers(object1, GameEventType.STAND_UP, toUserIds, id, tableId);
        }
        // 通知玩家排名
        JSONObject object2 = new JSONObject();
        object2.put("inner_id", innerId);
        object2.put("inner_cnt", innerCnt);
        object2.put("uid", userId);
        object2.put("ranking", ranking + "");
        object2.put("sn", Integer.parseInt(sitNum));
        object2.put("is_over", isOver ? "1" : "0");
        // 如果不捕获异常则会终端下一个任务
        GameUtils.notifyUser(object2, GameEventType.RANKING, userId, tableId);
    }

}
