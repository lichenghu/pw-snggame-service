package com.tiantian.snggame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.snggame.data.redis.RedisUtil;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.TableAllUser;
import com.tiantian.snggame.manager.model.TableStatus;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TableBeginHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String randomBtn = jsonObject.getString("randomBtn");
        if (randomBtn == null) {
            randomBtn = "";
        }
        // 从redis中加载桌子状态数据
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableStatus == null) {
            return;
        }
        // 如果不是准备则表示游戏已经在开始状态
        if (!GameStatus.READY.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser.getJoinTableUserMap() == null) {
            return;
        }
        // 第一次begin,判断人数是否齐了
        if (StringUtils.isNotBlank(randomBtn)
                && tableAllUser.getJoinTableUserMap().size() < Integer.parseInt(tableStatus.getMaxUsers())) { //判断人数
            return;
        }
        if(!tableStatus.isStarted()) { // 开始游戏标志
            tableStatus.setStarted("1");
            tableStatus.saveNotAddCnt();
        }
        Set<Map.Entry<String, String>> entrySet = tableAllUser.getJoinTableUserMap().entrySet();
        Iterator<Map.Entry<String, String>> iterator = entrySet.iterator();
        List<String> removeSitList = new ArrayList<>();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            String userId = entry.getValue();
            String userRoomTableKey = GameConstants.USER_SPINGO_TABLE_KEY + userId + ":" +tableId;
            String mapTableId = RedisUtil.getFromMap(userRoomTableKey, "tableId");
            if (mapTableId == null || !mapTableId.equalsIgnoreCase(tableId)) {
                removeSitList.add(entry.getKey());
                // 需要删除
                iterator.remove();
            }
        }

        if (removeSitList.size() > 0) {
            tableAllUser.flushGamingSitUser(removeSitList);
        }

        // 清除掉线站起的玩家
        GameUtils.clearStandUpOnlineUsers(tableAllUser.getOnlineTableUserIds(), tableId);

        tableStatus.setStatus(GameStatus.BEGIN.name());
        tableStatus.saveNotAddCnt();

        JSONObject data = new JSONObject();
        data.put(GameConstants.TASK_EVENT, GameStatus.D_AND_B.name());
        data.put("tableId", tableId);
        data.put("randomBtn", randomBtn);
        TableTaskEvent tableTaskEvent = new TableTaskEvent(GameStatus.D_AND_B.name(), data);
        Handlers.INSTANCE.execute(tableTaskEvent, self, context, sender);
    }
}
