package com.tiantian.snggame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.snggame.data.redis.RedisUtil;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.GameRecord;
import com.tiantian.snggame.manager.model.TableAllUser;
import com.tiantian.snggame.manager.model.TableStatus;
import com.tiantian.snggame.manager.model.TableUser;
import com.tiantian.snggame.manager.texas.Poker;
import com.tiantian.snggame.manager.texas.PokerManager;
import com.tiantian.snggame.utils.GameUtils;
import com.tiantian.snggame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TablePreFlopHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        // 回合ID
        String inningId = jsonObject.getString("inningId");
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        // 产生当局的牌
        List<Poker> pokerList = PokerManager.initPokers();
        Collection<String> userSits = tableAllUser.getGamingAndExitUserMap().keySet();
        Map<String, JSONObject> map = new HashMap<>();
        Map<String, String > userCardsMap = new HashMap<>();
        TableStatus tableStatus = TableStatus.load(tableId);
        if (!GameStatus.D_AND_B.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }

        GameRecord gameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (gameRecord == null) {
            gameRecord = new GameRecord();
        }
        List<GameRecord.Progress> progresses = gameRecord.getProgresses();
        progresses.add(GameRecord.Progress.create("pre_flop", "nil", "nil",
                System.currentTimeMillis() - gameRecord.getStartTime()));

        List<String> exitUserIds = new ArrayList<>();
        // 发底牌:每人发一张，发完后再每人发第二张
        for (int i = 0; i <= 1; i++) {
            for (String sitNum : userSits) {
                String userId = tableAllUser.getGamingAndExitUserMap().get(sitNum);
                if(StringUtils.isBlank(userId)) {
                    continue;
                }
                JSONObject object = map.get(userId);
                Poker poker = pokerList.remove(0);
                String userCards = null;
                if (object == null) {
                    object = new JSONObject();
                    userCards = poker.getShortPoker();
                    object.put("hand", userCards);
                } else {
                    userCards = (String) object.get("hand");
                    userCards += ("," + poker.getShortPoker());
                    object.put("hand", userCards);
                    progresses.add(GameRecord.Progress.create("deal", sitNum, userCards,
                            System.currentTimeMillis() - gameRecord.getStartTime()));
                }
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                // 保存玩家的底牌
                userCardsMap.put(sitNum, userCards);
                map.put(userId, object);

                String $uId = tableAllUser.getGamingSitUserMap().get(sitNum);
                if (StringUtils.isBlank($uId)) { // 玩家没有退出房间挂机
                    exitUserIds.add(userId);
                }
            }
        }
        if (exitUserIds.size() > 0) {
            for (String userId : exitUserIds) {
                 map.remove(userId);
            }
        }

        // 保存玩家底牌
        tableStatus.setUserCards(JSON.toJSONString(userCardsMap));
        // 获取当前需要下注的座位号
        String bigBlindSitNum = tableStatus.getBigBlindNum();
        String currentBetSitNum = GameUtils.getNextBetSitNum(tableStatus.canBetSits(), bigBlindSitNum, tableStatus.getMaxBetSitNum());

        // 设置当前需要下注人座位号
        tableStatus.setCurrentBet(currentBetSitNum);
        // 设置当前需要下注人座位号开始时间
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");
        // 设置当局的牌
        tableStatus.setCards(JSON.toJSONString(pokerList));
        // 设置状态
        tableStatus.setStatus(GameStatus.PRE_FLOP.name());


        // 校验是否直接进行下一个发牌
        GameStatus nextStatus = GameUtils.beforeCheckNextStatus(GameStatus.PRE_FLOP.name(), tableStatus);
        if (nextStatus != null) {
            // 保存但并不增加计数
            tableStatus.saveNotAddCnt();
            gameRecord.setProgresses(progresses);
            RecordUtils.restLastedRecord(gameRecord, tableId);
            // 触发下一轮的发牌事件
            GameUtils.nextStatusTask(nextStatus, tableId, inningId,
                    self, context, sender);
            return;
        }
        String pwd = tableStatus.randomPwd();
        // 刷新set的property值
        tableStatus.save();
        // 发送底牌信息给玩家
        sendToUsers(map, tableId);
        // 发送给观看的玩家
        sendToOnlines(tableAllUser.getOnlineTableUserIds(), tableAllUser.getGamingSitUserMap(),
                tableStatus.getInningId(), tableStatus.getIndexCount(), tableId);

        //获取第一个下注的人 发送给玩家通知下注
        String betUserId = tableAllUser.getGamingAndExitUserMap().get(currentBetSitNum);

        TableUser betTableUser = TableUser.load(betUserId, tableId);
        //增加玩家的操作计数(默认为1)
        RedisUtil.setMap(GameConstants.USER_SPINGO_TABLE_KEY + betUserId + ":" + tableId, "operateCount", "1");
        if (!betTableUser.isExit()) { // 玩家没有退出挂机
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", betUserId);
            object.put("sn", Integer.parseInt(currentBetSitNum));
            object.put("t", Long.parseLong(betTableUser.getTotalSecs()));
            String[] ops = tableStatus.getUserCanOps(betUserId, currentBetSitNum);
            object.put("c_b", StringUtils.join(ops, ","));
            object.put("pwd", pwd);
            GameUtils.notifyUser(object, GameEventType.BET, betUserId, tableId);
        }

        progresses.add(GameRecord.Progress.create("bet", betTableUser.getSitNum(), "nil",
                System.currentTimeMillis() - gameRecord.getStartTime()));
        gameRecord.setProgresses(progresses);
        RecordUtils.restLastedRecord(gameRecord, tableId);

        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", tableStatus.getIndexCount());
        otherNextObject.put("uid", betUserId);
        otherNextObject.put("sn", Integer.parseInt(currentBetSitNum));
        otherNextObject.put("t", Long.parseLong(betTableUser.getTotalSecs()));
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", "");
        try {
            Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
            userIds.remove(betUserId);
            // 如果不捕获异常则会终端下一个任务
            String newId = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableId);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        // 触发30s的检测任务 大盲注下一位的状态 如果已经操作则跳过, preFlop
        GameUtils.triggerBetTestTask(tableId, inningId, currentBetSitNum, betUserId, 1, pwd, betTableUser.isExit(),
                Long.parseLong(betTableUser.getTotalSecs()));
    }

    private void sendToUsers(Map<String, JSONObject> map, String tableId) {
        for (Map.Entry<String, JSONObject> entry : map.entrySet()) {
            String userId = entry.getKey();
            JSONObject object = entry.getValue();
            GameUtils.notifyUser(object, GameEventType.PREFLOP, userId, tableId);
        }
    }
    private void sendToOnlines(Collection<String> onlineUserIds, Map<String, String> gamingMap,
                               String innerId, String innerCnt, String tableId) {
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", innerCnt);
        object.put("hand", "");
        for (String userId : onlineUserIds) {
            if (!gamingMap.containsValue(userId)) {
                GameUtils.notifyUser(object, GameEventType.PREFLOP, userId, tableId);
            }
        }
    }
}
