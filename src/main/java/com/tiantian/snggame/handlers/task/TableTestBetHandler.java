package com.tiantian.snggame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.sng.akka.event.user.TableUserAllinEvent;
import com.tiantian.sng.akka.event.user.TableUserCallEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import com.tiantian.snggame.utils.RecordUtils;
import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 *
 */
public class TableTestBetHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        String sitNum = jsonObject.getString("sitNum");
        String userId = jsonObject.getString("userId");
        String operateCount = jsonObject.getString("operateCount");
        String pwdParam = jsonObject.getString("pwd");
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null) {
            return;
        }
        if (!tableStatus.checkPwd(pwdParam)) {
            return;
        }
        //  牌局已经不是当前牌局
        if (!inningId.equals(tableStatus.getInningId())) {
            return;
        }
        //  牌局已经结束
        if (GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            return;
        }

        String currentBet = tableStatus.getCurrentBet();
        // 已经当前需要下注的不是任务中的人
        if (currentBet == null || !currentBet.equals(sitNum)) {
            return;
        }
        // 判断是否弃牌
        Map<String, String> allNotFolds = tableStatus.allNotFoldSitNumsAndCards();
        //获取桌子玩家人数
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
        String status = null;
        // 没有弃牌
        if (allNotFolds != null && allNotFolds.containsKey(sitNum)) {
            String tableUserId = tableAllUser.getGamingAndExitUserMap().get(sitNum);
            // 位置上的人不是当前桌子座位玩家，可能任务的玩家已经退出了
            if (tableUserId == null || !tableUserId.equals(userId)) {
                return;
            }
            TableUser tableUser = TableUser.load(userId, tableId);
            String userTableId = tableUser.getTableId();
            String userSitNum = tableUser.getSitNum();
            String userOperateCount = tableUser.getOperateCount();
            if (StringUtils.isBlank(userOperateCount)) {
                tableUser.setOperateCount("1");
            }
            if (!tableId.equals(userTableId)) {
                return;
            }
            if (!sitNum.equals(userSitNum)) {
                return;
            }
            // 玩家当前操作序号大于检测时候的版本号说明已经玩家操作过一次，该次检测跳过
            if (userOperateCount != null && (Integer.parseInt(userOperateCount) > Integer.parseInt(operateCount))) {
                return;
            }

            UserChips currUserChips = UserChips.load(userId, tableUser.getTableId());
            String[] currOps = tableStatus.getUserCanOps(userId, userSitNum);

            char op = currOps[0].charAt(1);
            // 可以让牌
            if ('1' == op) {
                status = GameEventType.CHECK;
            } else {
                status = GameEventType.FOLD;
            }

            if (tableUser.isExit() || (tableUser.getTotalSecs() != null &&
                                Integer.parseInt(tableUser.getTotalSecs()) < GameConstants.BET_DELAYER_TIME / 1000)) { // 玩家已经退出房间,或者挂机
                UserChips userChips = UserChips.load(userId, tableId);
                // 当玩家筹码小于等于一个大盲注的时候自动跟注或者allin
                if (userChips != null && userChips.getChips() <= Long.parseLong(tableStatus.getBigBlindMoney())) {
                    char canCall = currOps[0].charAt(2);
                    if ('1' == canCall) {  // 判断能不能跟注
                        //call
                        Handlers.INSTANCE.execute(new TableUserCallEvent(event.tableId(), userId, pwdParam),
                                self, context, sender);
                        return;
                    }
                    char canAllin = currOps[0].charAt(4);
                    if ('1' == canAllin) {
                        // allin
                        Handlers.INSTANCE.execute(new TableUserAllinEvent(event.tableId(), userId, pwdParam),
                                self, context, sender);
                        return;
                    }
                }
            }
            long smallMoney = Long.parseLong(tableStatus.getSmallBlindMoney());
            long buyIn = Long.parseLong(tableStatus.getBuyIn());
            if (smallMoney >= buyIn / 2) {
                Handlers.INSTANCE.execute(new TableUserAllinEvent(tableId, userId, pwdParam),
                        self, context, sender);
                // 增加玩家的强制操作
                String oldNotOpOperateCount = tableUser.getNotOperateCount();
                if (StringUtils.isBlank(oldNotOpOperateCount)) {
                    oldNotOpOperateCount = "0";
                }
                // 增加玩家的操作计数
                tableUser.setNotOperateCount((Integer.parseInt(oldNotOpOperateCount) + 1) + "");
                // 玩家几次没有操作
                if (Integer.parseInt(tableUser.getNotOperateCount()) >= GameConstants.MAX_NOT_OPERATE_COUNT) {
                    //减少CD
                    long userTotalSecs = Long.parseLong(tableUser.getTotalSecs());
                    if (userTotalSecs > 3) { // 2次没操作直接修改成3s的下注时间
                        userTotalSecs = 3;
                    }
                    tableUser.setTotalSecs(tableUser.isExit() ? 1 + "" : userTotalSecs + "");
                }
                // 刷新setProperty 数据
                tableUser.save();
                return;
            }

            // 增加玩家的操作计数
            tableUser.addOneOperateCount();
            // 增加玩家的强制操作（当强制玩家2次后，玩家自动站起）
            String oldNotOpOperateCount = tableUser.getNotOperateCount();

            tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);

            if (StringUtils.isBlank(oldNotOpOperateCount)) {
                oldNotOpOperateCount = "0";
            }
            // 增加玩家的操作计数
            tableUser.setNotOperateCount((Integer.parseInt(oldNotOpOperateCount) + 1) + "");
            tableUser.setBetStatus(status);
            //玩家几次没有操作
            if (Integer.parseInt(tableUser.getNotOperateCount()) >= GameConstants.MAX_NOT_OPERATE_COUNT) {
                //减少CD
                long userTotalSecs = Long.parseLong(tableUser.getTotalSecs());
                if (userTotalSecs > 3) { // 2次没操作直接修改成5s的下注时间
                    userTotalSecs = 3;
                }
                tableUser.setTotalSecs(tableUser.isExit() ? 1 + "" : userTotalSecs + "");
            }

            // 通知其他人 该玩家的下注状态  看牌, 弃牌
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(sitNum));
            object.put("left_chips", currUserChips.getChips());
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, status, userIds, id, tableId);

            // 刷新setProperty 数据
            tableUser.save();
        }

        //测试游戏状态: 是否进行下流程: flop turn river fish
        Set<String> canBetSits = tableStatus.canBetSits();
        String maxBetSitNum = tableStatus.getMaxBetSitNum();

        String currentStatus = tableStatus.getStatus();

        GameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (mttGameRecord == null) {
            mttGameRecord = new GameRecord();
        }
        List<GameRecord.Progress> progresses = mttGameRecord.getProgresses();
        progresses.add(GameRecord.Progress.create(status, sitNum,  "nil",
                System.currentTimeMillis() - mttGameRecord.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord, tableId);

        GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, currentStatus, maxBetSitNum, sitNum);
        if (nextStatus != null) {
            //完成一轮的下注结算，分池
            tableStatus.roundBetEnd();
            if (status != null && (status.equalsIgnoreCase(GameEventType.FOLD) ||
                    status.equalsIgnoreCase(GameEventType.ALLIN))) {
                boolean needShow = tableStatus.needShowAllCards();
                if (needShow) { // 需要显示所有的手牌
                    List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                    JSONObject object2 = new JSONObject();
                    object2.put("inner_id", tableStatus.getInningId());
                    object2.put("inner_cnt", tableStatus.getIndexCount());
                    object2.put("all_cards", cardsList);
                    String id2 = UUID.randomUUID().toString().replace("-", "");
                    GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                }
            }
            // 保存数据
            tableStatus.save();
            // 通知玩家的池信息
            GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
            // 触发下一轮的发牌事件
            GameUtils.triggerNextStatusTask(nextStatus, tableId, inningId);
            return;
        }

        // 触发下一个玩家的30s定时检测下注任务
        // 获取下个下注的玩家座位号
        String nextBetSitNum = GameUtils.getNextBetSitNum(canBetSits, sitNum, maxBetSitNum);
        String nextBetUserId = tableAllUser.getGamingAndExitUserMap().get(nextBetSitNum);
        TableUser nextTableUser = TableUser.load(nextBetUserId, tableId);

        String nextUserOperateCount = nextTableUser.getOperateCount();
        if (StringUtils.isBlank(nextUserOperateCount)) {
            nextUserOperateCount = "1";
            nextTableUser.setOperateCount(nextUserOperateCount);
            nextTableUser.save();
        }
        // 设置当前需要下注的人的座位号
        tableStatus.setCurrentBet(nextBetSitNum);
        tableStatus.setCurrentBetTimes(System.currentTimeMillis() + "");
        String pwd = tableStatus.randomPwd();
        tableStatus.save();

        GameRecord mttGameRecord2 = RecordUtils.getLastedRecord(tableStatus.getTableId());
        if (mttGameRecord2 == null) {
            mttGameRecord2 = new GameRecord();
        }
        List<GameRecord.Progress> progresses2 = mttGameRecord2.getProgresses();
        progresses2.add(GameRecord.Progress.create("bet", sitNum,  "nil",
                System.currentTimeMillis() - mttGameRecord2.getStartTime()));
        RecordUtils.restLastedRecord(mttGameRecord2, tableId);

        if(!nextTableUser.isExit()) { // 玩家没有退出挂机
            // 通知玩家下注
            JSONObject nextObject = new JSONObject();
            nextObject.put("inner_id", tableStatus.getInningId());
            nextObject.put("inner_cnt", tableStatus.getIndexCount());
            nextObject.put("uid", nextBetUserId);
            nextObject.put("sn", Integer.parseInt(nextBetSitNum));
            nextObject.put("t", Long.parseLong(nextTableUser.getTotalSecs()));
            String[] ops = tableStatus.getUserCanOps(nextBetUserId, nextBetSitNum);
            nextObject.put("c_b", StringUtils.join(ops, ",")); // 玩家可以操作的选项
            nextObject.put("pwd", pwd);
            // 如果不捕获异常则会终端下一个任务
            GameUtils.notifyUser(nextObject, GameEventType.BET, nextBetUserId, tableId);
        }
        // 通知其他玩家下注玩家需要下注
        JSONObject otherNextObject = new JSONObject();
        otherNextObject.put("inner_id", tableStatus.getInningId());
        otherNextObject.put("inner_cnt", tableStatus.getIndexCount());
        otherNextObject.put("uid", nextBetUserId);
        otherNextObject.put("sn", Integer.parseInt(nextBetSitNum));
        otherNextObject.put("t", Long.parseLong(nextTableUser.getTotalSecs()));
        otherNextObject.put("c_b", "");
        otherNextObject.put("pwd", pwd);
        userIds.remove(nextBetUserId);
        // 如果不捕获异常则会终端下一个任务
        String newId = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(otherNextObject, GameEventType.BET, userIds, newId, tableId);
        int newOperateCount = Integer.parseInt(nextUserOperateCount);
        // 触发30s的检测任务 大盲注下一位的状态 如果已经操作则跳过, preFlop
        GameUtils.triggerBetTestTask(tableId, inningId, nextBetSitNum, nextBetUserId, newOperateCount, pwd, nextTableUser.isExit(),
                Long.parseLong(nextTableUser.getTotalSecs()));
    }
}
