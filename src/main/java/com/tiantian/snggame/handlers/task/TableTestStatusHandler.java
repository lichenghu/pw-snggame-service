package com.tiantian.snggame.handlers.task;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.utils.GameUtils;

/**
 *
 */
public class TableTestStatusHandler implements EventHandler<TableTaskEvent> {
    @Override
    public void handler(TableTaskEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        JSONObject jsonObject = event.getParams();
        String tableId = jsonObject.getString("tableId");
        String inningId = jsonObject.getString("inningId");
        String nextStatus = jsonObject.getString("selector");
        // 触发下一轮的发牌事件
        GameUtils.nextStatusTask(GameStatus.valueOf(nextStatus), tableId, inningId, self, context, sender);
    }
}
