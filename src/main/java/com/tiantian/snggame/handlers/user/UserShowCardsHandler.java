package com.tiantian.snggame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;

import com.tiantian.sng.akka.event.user.TableUserShowCardsEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.TableAllUser;
import com.tiantian.snggame.manager.model.TableStatus;
import com.tiantian.snggame.manager.model.TableUser;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;

import java.util.UUID;

/**
 *
 */
public class UserShowCardsHandler implements EventHandler<TableUserShowCardsEvent> {
    @Override
    public void handler(TableUserShowCardsEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        String hands = event.getData();
        TableUser tableUser = TableUser.load(userId, event.tableId());
        if (tableUser == null) {
            return;
        }
        TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
        // 是在结算的时候直接亮牌
        if (tableStatus != null && GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus())) {
            String handCardsStr = tableStatus.getUserHandCards(tableUser.getSitNum());
            if (StringUtils.isBlank(handCardsStr)) {
                return;
            }
            String[] handCardStrs = handCardsStr.split(",");
            String handCards = handCardsStr;
            if (StringUtils.isNotBlank(hands)) {
                if ("1".equalsIgnoreCase(hands)) {
                    handCards =  handCardStrs[0] + ",";
                } else if ("2".equalsIgnoreCase(hands)) {
                    handCards = "," + handCardStrs[1];
                }
            }
            TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
            JSONObject object = new JSONObject();
            object.put("inner_id", tableStatus.getInningId());
            object.put("inner_cnt", tableStatus.getIndexCount());
            object.put("uid", userId);
            object.put("sn", Integer.parseInt(tableUser.getSitNum()));
            object.put("hand_cards", handCards);
            String id = UUID.randomUUID().toString().replace("-", "");
            GameUtils.notifyUsers(object, GameEventType.SHOW_CARDS, tableAllUser.getOnlineTableUserIds(), id, tableId);
        }
    }
}
