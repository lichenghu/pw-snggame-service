package com.tiantian.snggame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.user.TableUserFoldEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import com.tiantian.snggame.utils.RecordUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserFoldHandler implements EventHandler<TableUserFoldEvent> {
    @Override
    public void handler(TableUserFoldEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.tableId();
        TableUser tableUser = TableUser.load(userId, event.tableId());
        if (tableUser.getSitNum() != null) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                //设置玩家弃牌
                tableUser.setBetStatus(GameEventType.FOLD);
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableUser.save();

                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), GameEventType.FOLD);
                UserChips userChips = UserChips.load(userId, tableUser.getTableId());
                // 通知其他在玩家 （暂未实现通知所有玩家）
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                object.put("left_chips", userChips.getChips());
                //发送个桌子的玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, GameEventType.FOLD, userIds, id, tableId);

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", GameEventType.FOLD);
                GameUtils.notifyUser(object1, "call_suc", userId, tableId);

                GameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
                if (mttGameRecord == null) {
                    mttGameRecord = new GameRecord();
                }
                List<GameRecord.Progress> progresses = mttGameRecord.getProgresses();
                progresses.add(GameRecord.Progress.create("fold", tableUser.getSitNum(),  "nil",
                        System.currentTimeMillis() - mttGameRecord.getStartTime()));
                RecordUtils.restLastedRecord(mttGameRecord, tableId);

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean needShow = tableStatus.needShowAllCards();
                    if (needShow) { // 需要显示所有的手牌
                        List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                        JSONObject object2 = new JSONObject();
                        object2.put("inner_id", tableStatus.getInningId());
                        object2.put("inner_cnt", tableStatus.getIndexCount());
                        object2.put("all_cards", cardsList);
                        String id2 = UUID.randomUUID().toString().replace("-", "");
                        GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                    }

                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId());
                    return;
                }

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, null, false, null,
                        self, context, sender);
            }
        }
    }
}
