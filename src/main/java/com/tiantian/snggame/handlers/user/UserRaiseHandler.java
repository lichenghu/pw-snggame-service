package com.tiantian.snggame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.user.TableUserAllinEvent;
import com.tiantian.sng.akka.event.user.TableUserCheckEvent;
import com.tiantian.sng.akka.event.user.TableUserFoldEvent;
import com.tiantian.sng.akka.event.user.TableUserRaiseEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import com.tiantian.snggame.utils.RecordUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserRaiseHandler implements EventHandler<TableUserRaiseEvent> {
    @Override
    public void handler(TableUserRaiseEvent userRaiseEvent, ActorRef self, UntypedActorContext context, ActorRef sender) {

        String userId = userRaiseEvent.getUserId();
        long raise = userRaiseEvent.getRaise();
        String tableId = userRaiseEvent.tableId();
        String pwd = userRaiseEvent.getPwd();
        TableUser tableUser = TableUser.load(userId, userRaiseEvent.tableId());
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            if(raise < 0) {
                System.out.println("raise is zero : "+ JSON.toJSONString(tableStatus));
            }
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                //校验玩家能否加注，然后加注
                UserChips userChips = UserChips.load(userId, tableUser.getTableId());

                String[] ops = tableStatus.getUserCanOps(userId, tableUser.getSitNum());
                String status = null;
                char op = ops[0].charAt(3);
                long minRaiseChips = Long.parseLong(ops[1]);
                // 能不能加注
                if ('0' == op || minRaiseChips > raise) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        Handlers.INSTANCE.execute(new TableUserFoldEvent(userRaiseEvent.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        Handlers.INSTANCE.execute(new TableUserCheckEvent(userRaiseEvent.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    }
                } else {
                    if (raise >= userChips.getChips()) {
                        raise = userChips.getChips();
                        status = GameEventType.ALLIN;
                        Handlers.INSTANCE.execute(new TableUserAllinEvent(userRaiseEvent.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    } else {
                        status = GameEventType.RAISE;
                    }
                }

                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                if (status.equalsIgnoreCase(GameEventType.RAISE) ||
                        status.equalsIgnoreCase(GameEventType.ALLIN)) {
                    userChips.reduceAndFlushChips(raise);
                    object.put("val", raise);
                    tableStatus.userBet(tableUser.getSitNum(), raise);
                    // 更新平均加注日志
                    tableStatus.updateUserBetRaiseLog(tableUser.getSitNum(), raise);
                }
                object.put("left_chips", userChips.getChips());
                object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                //设置玩家加注
                tableUser.setBetStatus(status);
                tableUser.save();

                // 通知其他在玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableId);

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableId);


                GameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
                if (mttGameRecord == null) {
                    mttGameRecord = new GameRecord();
                }
                List<GameRecord.Progress> progresses = mttGameRecord.getProgresses();
                progresses.add(GameRecord.Progress.create("raise", tableUser.getSitNum(),  raise + "",
                        System.currentTimeMillis() - mttGameRecord.getStartTime()));
                RecordUtils.restLastedRecord(mttGameRecord, tableId);


                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    if(status.equalsIgnoreCase(GameEventType.ALLIN)
                            || status.equalsIgnoreCase(GameEventType.FOLD)) {
                        boolean needShow = tableStatus.needShowAllCards();
                        if (needShow) { // 需要显示所有的手牌
                            List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                            JSONObject object2 = new JSONObject();
                            object2.put("inner_id", tableStatus.getInningId());
                            object2.put("inner_cnt", tableStatus.getIndexCount());
                            object2.put("all_cards", cardsList);
                            String id2 = UUID.randomUUID().toString().replace("-", "");
                            GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                        }
                    }
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    tableStatus.save();
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId());
                    return;
                }
                // 玩家下的最大注
                String maxBetSitNumChips = tableStatus.getMaxBetSitNumChips();
                String maxBetSitNum = tableStatus.getMaxBetSitNum();

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, maxBetSitNum, false, maxBetSitNumChips,
                        self, context, sender);
            }
        }
    }
}
