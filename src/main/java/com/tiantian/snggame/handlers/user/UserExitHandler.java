package com.tiantian.snggame.handlers.user;

import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.user.TableUserExitEvent;
import com.tiantian.snggame.handlers.EventRetHandler;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

/**
 *
 */
public class UserExitHandler implements EventRetHandler<TableUserExitEvent> {
    static Logger LOG = LoggerFactory.getLogger(UserExitHandler.class);
    @Override
    public Object handler(TableUserExitEvent event) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        TableStatus tableStatus = TableStatus.load(tableId);
        boolean isTrueExit = false;
        TableUser tableUser = TableUser.load(userId, tableId);
        if ((tableStatus != null && !tableStatus.isStarted()) ||
                (tableUser != null && "preparing".equalsIgnoreCase(tableUser.getStatus()))) { //游戏没有开始
            isTrueExit = true;
        }
        LOG.info("isTrueExit is :" + isTrueExit);

        if (tableStatus != null) {
            // 移除在线玩家
            RoomUsers.removeOnlineUsers(tableStatus.getRoomId(), userId);
        }
        if (tableUser == null || tableUser.isNull() ) {
            LOG.info("tableUser is null");
            return isTrueExit ? "true_exit" : "false_exit";
        }
        // 通知其他人 该玩家离开
        TableAllUser tableAllUsers = TableAllUser.load(tableId);
        if (isTrueExit) {
           exitRoom(tableStatus, tableAllUsers, userId, tableUser.getSitNum(),
                    tableUser.getNickName());
           tableUser.delSelf(tableId);
           UserChips userChips = UserChips.load(userId, tableId);
           if (userChips != null) {
               userChips.delSelf(tableId);
           }
           tableAllUsers.deleteUser(tableUser.getSitNum());
        }
        else {
            LOG.info("exit false_exit");
            String userStatus = tableUser.getStatus();
            UserChips userChips = UserChips.load(userId, tableId);
            if ("standing".equalsIgnoreCase(userStatus) ||
                    (userChips != null && userChips.getChips() == 0    // 玩家还在等待checkChips流程中间退出
                          && tableStatus != null &&
                            GameStatus.FINISH.name().equalsIgnoreCase(tableStatus.getStatus()))) { //玩家已经结束, UserChip 已经被删除
                tableAllUsers.deleteUserByUserId(tableUser.getUserId());
                exitRoom(tableStatus, tableAllUsers, userId, tableUser.getSitNum(),
                         tableUser.getNickName());
                tableUser.delSelf(tableId);
                LOG.info("exit false_exit standing");

                return "false_exit";
            }
            tableUser.setStatus("exit");
            tableUser.save();
            exitRoom(tableStatus, tableAllUsers, userId, tableUser.getSitNum(),
                    tableUser.getNickName());
            LOG.info("exit false_exit exit");
            if (StringUtils.isBlank(tableUser.getSitNum())) { // 已经站起了 不发送退出消息给其他人
                LOG.info("false_exit");
                return "false_exit";
            }
        }
        return isTrueExit ? "true_exit" : "false_exit";
    }

    private void exitRoom(TableStatus tableStatus, TableAllUser tableAllUsers, String userId, String sitNum,
                          String nickName) {
        if (StringUtils.isBlank(sitNum) || tableStatus.isStarted()) { //游戏开始后不发送退出消息
            return;
        }
        // 通知其他人 该玩家离开
        JSONObject object = new JSONObject();
        object.put("inner_id", tableStatus.getInningId());
        object.put("inner_cnt", tableStatus.getIndexCount());
        object.put("uid", userId);
        object.put("sn", Integer.parseInt(sitNum));
        object.put("nick_name", nickName);
        String id = UUID.randomUUID().toString().replace("-", "");
        GameUtils.notifyUsers(object, GameEventType.EXIT_ROOM,
                tableAllUsers.getOnlineTableUserIds(), id, tableStatus.getTableId());
    }
}
