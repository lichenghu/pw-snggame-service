package com.tiantian.snggame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.sng.akka.event.user.TableUserRejoinEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.manager.texas.PokerManager;
import com.tiantian.snggame.manager.texas.PokerOuts;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.*;

/**
 *
 */
public class UserRejoinHandler implements EventHandler<TableUserRejoinEvent> {
    @Override
    public void handler(TableUserRejoinEvent event, ActorRef self,
                        UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        TableUser tableUser = TableUser.load(userId, tableId);
        TableStatus tableStatus = TableStatus.load(tableId);
        if(tableUser.isNull() || tableStatus == null) {
            // 通知玩家游戏已经结束
            JSONObject object1 = new JSONObject();
            object1.put("game_over", true);
            GameUtils.notifyUser(object1, "game_over", userId, tableId);
            return;
        }
        RoomUsers.addOnlineUsers(tableUser.getRoomId(), userId);
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        if (tableAllUser != null) {
            UserInfHandlerHelper.sendUserInfos(tableStatus, tableAllUser, tableId, userId, tableUser.getNickName()
                    , tableUser.getAvatarUrl(), tableUser.getGender());
        }
    }
}
