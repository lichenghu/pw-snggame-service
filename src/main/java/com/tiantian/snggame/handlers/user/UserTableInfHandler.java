package com.tiantian.snggame.handlers.user;

import com.alibaba.fastjson.JSON;
import com.tiantian.core.proxy_client.AccountIface;
import com.tiantian.core.thrift.account.UserDetail;
import com.tiantian.sng.akka.event.user.TableUserTableInfEvent;
import com.tiantian.sng.akka.result.TableInfoResult;
import com.tiantian.sng.akka.result.UserBetResult;
import com.tiantian.sng.akka.result.UserInfoResult;
import com.tiantian.sng.akka.result.UserStatusResult;
import com.tiantian.snggame.handlers.EventRetHandler;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.manager.texas.PokerManager;
import com.tiantian.snggame.manager.texas.PokerOuts;
import org.apache.commons.lang.StringUtils;
import org.apache.thrift.TException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class UserTableInfHandler implements EventRetHandler<TableUserTableInfEvent> {

    @Override
    public Object handler(TableUserTableInfEvent event) {
        String userId = event.getUserId();
        String tableId = event.tableId();
        TableInfoResult tableInfo = new TableInfoResult();
        System.out.println("-tableId------------" + tableId);
        TableUser tableUser = TableUser.load(userId, tableId);
        System.out.println("tableUser:"+ JSON.toJSONString(tableUser));
//        if (tableUser.isNull()) {
//            return tableInfo;
//        }
//        System.out.println("-userTableId------------" + userTableId);
//        if (StringUtils.isBlank(userTableId)) {
//            return tableInfo;
//        }
        TableStatus tableStatus = TableStatus.load(tableId);
        System.out.println("tableStatus:"+ JSON.toJSONString(tableStatus));
        if (tableStatus == null) {
            return null;
        }
        int btn = -1;
        if (StringUtils.isNotBlank(tableStatus.getButton())) {
            btn = Integer.parseInt(tableStatus.getButton());
        }
        tableInfo.setButton(btn);
        int small = -1;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindNum())) {
            small = Integer.parseInt(tableStatus.getSmallBlindNum());
        }
        tableInfo.setSmallBtn(small);
        int smallMoney = 0;
        if (StringUtils.isNotBlank(tableStatus.getSmallBlindMoney())) {
            smallMoney = Integer.parseInt(tableStatus.getSmallBlindMoney());
        }
        tableInfo.setSmallBtnMoney(smallMoney);
        int big = -1;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindNum())) {
            big = Integer.parseInt(tableStatus.getBigBlindNum());
        }
        tableInfo.setBigBtn(big);
        int bigMoney = 0;
        if (StringUtils.isNotBlank(tableStatus.getBigBlindMoney())) {
            bigMoney = Integer.parseInt(tableStatus.getBigBlindMoney());
        }
        tableInfo.setBigBtnMoney(bigMoney);
        TableAllUser tableAllUser = TableAllUser.load(tableId);

        Map<String, String> userGamingMap = null;
        if(tableStatus.isStarted()) {
            userGamingMap = tableAllUser.getGamingAndExitUserMap();
        } else {
            userGamingMap = tableAllUser.getJoinTableUserMap();
        }

        // 判断当前玩家是否正在游戏里面
        List<UserInfoResult> userInfoList = getUserInfos(userGamingMap, tableId, userId);
        tableInfo.setUsers(userInfoList);
        List<Long> poolList = tableStatus.betPoolList();
        tableInfo.setPool(StringUtils.join(poolList, ","));

        int currentBet = 0;
        String betUserId = null;
        Map<String, String> gamingMap = tableAllUser.getGamingAndExitUserMap();
        if (StringUtils.isNotBlank(tableStatus.getCurrentBet())) {
            currentBet = Integer.valueOf(tableStatus.getCurrentBet());
            betUserId = gamingMap.get(tableStatus.getCurrentBet());
        }
        TableUser betTableUser = null;
        if (StringUtils.isNotBlank(betUserId)) {
            betTableUser = TableUser.load(betUserId, tableId);
        }
        tableInfo.setCurBetSit(currentBet);

        String currentBetTimes = tableStatus.getCurrentBetTimes();
        long leftSecs = 0;
        if (StringUtils.isNotBlank(currentBetTimes)) {
            String status = tableStatus.getSitBetStatusBySitNum(tableStatus.getCurrentBet());
            if (status != null && !GameEventType.FOLD.equalsIgnoreCase(status)) {
                long times = System.currentTimeMillis() - Long.parseLong(currentBetTimes);
                long total = GameConstants.BET_DELAYER_TIME;
                if (betTableUser != null) {
                    total = Long.parseLong(betTableUser.getTotalSecs()) * 1000;
                }
                leftSecs = Math.max(0, total - times);
            }
        }
        tableInfo.setLeftSecs(leftSecs/1000);

        String usersBetsLog = tableStatus.getUsersBetsLog();
        List<UserBetResult> userBets = new ArrayList<>();
        if (StringUtils.isNotBlank(usersBetsLog)) {
            List<UserBetLog> userBetLogs = JSON.parseArray(usersBetsLog, UserBetLog.class);
            for (UserBetLog userBetLog : userBetLogs) {
                if (!gamingMap.containsKey(userBetLog.getSitNum())) {
                    continue;
                }
                UserBetResult userBet = new UserBetResult();
                userBet.setSn(Integer.valueOf(userBetLog.getSitNum()));
                userBet.setBet(userBetLog.getRoundChips());
                userBets.add(userBet);
            }
        }
        tableInfo.setUserBets(userBets);
        long totalSec = 0;
        if (betTableUser != null) {
            if (!betTableUser.isNull()) {
                String totalSecStr = betTableUser.getTotalSecs();
                if (StringUtils.isBlank(totalSecStr)) {
                    totalSec = GameConstants.BET_DELAYER_TIME / 1000l;
                }
                else {
                    totalSec = Long.parseLong(totalSecStr);
                }
            }
        }
        tableInfo.setTotalSecs(totalSec);
        // 牌局信息
        if (StringUtils.isNotBlank(tableStatus.getDeskCards())) {
            List<String> deskCards = JSON.parseObject(tableStatus.getDeskCards(), List.class);
            tableInfo.setDeskCards(StringUtils.join(deskCards, ","));
        }
        List<Map<String, Object>> sitBetStatusMap = tableStatus.allUserBetStatus();
        List<UserStatusResult> userStatuses = new ArrayList<>();

        // 设置大盲注状态，开始时候默认设置了大盲注状态
        UserBetLog bigUserBetLog = tableStatus.getUserBetLog(tableStatus.getBigBlindNum());
        if(bigUserBetLog != null) {
            long chips = bigUserBetLog.getRoundChips() + bigUserBetLog.getTotalChips();
            if (chips != Long.parseLong(tableStatus.getBigBlindMoney())) { // 玩家没有下过注
                String bigStatus = tableStatus.getSitBetStatusBySitNum(tableStatus.getBigBlindNum());
                // 状态为初始状态
                if (GameEventType.CALL.equalsIgnoreCase(bigStatus)) {
                    for (Map<String, Object> map : sitBetStatusMap) {
                         if (map.containsKey(tableStatus.getBigBlindNum())) {
                             map.put(tableStatus.getBigBlindNum(), "");
                             break;
                         }
                    }
                }
            }
        }

        if (sitBetStatusMap != null) {
            for (Map<String, Object> statusMap : sitBetStatusMap) {
                UserStatusResult userStatus = new UserStatusResult();
                Integer sn = (Integer)statusMap.get("sn");
                String status = (String)statusMap.get("status");
                // 在游戏中
                if (gamingMap.containsKey(sn.toString())) {
                    userStatus.setSn(sn);
                    userStatus.setStatus(status);
                    userStatuses.add(userStatus);
                }
            }
        }

        tableInfo.setUserStatus(userStatuses);
        String userCards = "";
        Map<String, String> userCardsMap = tableStatus.getUsersCards();
        String userCanOps = "";
        String cardLevel = "";
        if (userCardsMap != null) {
            String sitNum = tableUser.getSitNum();
            if (StringUtils.isNotBlank(sitNum)) {
                userCards = userCardsMap.get(sitNum);
                if (sitNum.equalsIgnoreCase(tableStatus.getCurrentBet())) {
                    String[] ops = tableStatus.getUserCanOps(tableUser.getUserId(), sitNum);
                    userCanOps = StringUtils.join(ops, ",");
                }
                PokerOuts pokerOuts = PokerManager.getPokerOuts(userCards, tableStatus.getDeskCardList());
                if (pokerOuts != null) {
                    cardLevel = pokerOuts.getLevel() + "";
                }
            }
        }
        tableInfo.setHandCards(userCards == null ? "" : userCards);
        tableInfo.setCardLevel(cardLevel);
        tableInfo.setUserCanOps(userCanOps);
        tableInfo.setPwd(StringUtils.isBlank(userCanOps) || tableStatus.getPwd() == null  ? "" : tableStatus.getPwd());
        String innerId = tableStatus.getInningId() == null ? "" : tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount() == null ? "0" : tableStatus.getIndexCount();
        tableInfo.setInnerId(innerId);
        tableInfo.setInnerCnt(indexCount);
        tableInfo.setGameStatus(StringUtils.isBlank(tableStatus.getStatus()) ? "" : tableStatus.getStatus().toLowerCase());
        tableInfo.setWaitNums(Integer.parseInt(tableStatus.getMaxUsers()) - tableAllUser.getJoinTableUserMap().size());
        tableInfo.setRanking(tableUser == null || StringUtils.isBlank(tableUser.getRanking())
                ? "0" : tableUser.getRanking());
        return tableInfo;
    }

    private List<UserInfoResult> getUserInfos(Map<String, String> gamingSitUserMap, String tableId,
                                        String userId) {
        List<UserInfoResult> userInfos = new ArrayList<>();
        boolean isStandUp = true;
        if (gamingSitUserMap != null) {
            Set<Map.Entry<String, String>> entries = gamingSitUserMap.entrySet();
            for (Map.Entry<String, String> entry : entries) {
                UserInfoResult userInfo = new UserInfoResult();
                String $sitNum = entry.getKey();
                String $userId = entry.getValue();
                if ($userId.equalsIgnoreCase(userId)) {
                    isStandUp = false;
                }
                TableUser tableUser = TableUser.load($userId, tableId);
                if (tableUser == null) {
                    continue;
                }
                String $avatarUrl = "";
                if (tableUser.getAvatarUrl() != null) {
                    $avatarUrl = tableUser.getAvatarUrl();
                }
                String $nickName = "匿名";
                if (tableUser.getNickName() != null) {
                    $nickName = tableUser.getNickName();
                }
                String gender = "0";
                if (StringUtils.isNotBlank(tableUser.getGender())) {
                    gender = tableUser.getGender();
                }
                userInfo.setUserId($userId);
                userInfo.setSitNum(Integer.parseInt($sitNum));
                userInfo.setNickName($nickName);
                userInfo.setAvatarUrl($avatarUrl);
                userInfo.setPlaying(1);
                userInfo.setGender(gender);
                UserChips $userChips = UserChips.load($userId, tableId);
                if ($userChips != null) {
                    userInfo.setMoney($userChips.getChips());
                }
                userInfos.add(userInfo);
            }
        }
        // 玩家是站起状态
//        if (isStandUp) {
//            UserInfoResult userInfo = new UserInfoResult();
//            TableUser tableUser = TableUser.load(userId, tableId);
//            if (tableUser != null) {
//                String $avatarUrl = "";
//                if (tableUser.getAvatarUrl() != null) {
//                    $avatarUrl = tableUser.getAvatarUrl();
//                }
//                String $nickName = "匿名";
//                if (tableUser.getNickName() != null) {
//                    $nickName = tableUser.getNickName();
//                }
//                String gender = "0";
//                if (StringUtils.isNotBlank(tableUser.getGender())) {
//                    gender = tableUser.getGender();
//                }
//                userInfo.setUserId(userId);
//                userInfo.setSitNum(-1);
//                userInfo.setNickName($nickName);
//                userInfo.setAvatarUrl($avatarUrl);
//                userInfo.setPlaying(0);
//                userInfo.setGender(gender);
//                UserChips $userChips = UserChips.load(userId, tableId);
//                if ($userChips != null) {
//                    userInfo.setMoney($userChips.getChips());
//                }
//                userInfos.add(userInfo);
//            }
//        }
        return userInfos;
    }
}
