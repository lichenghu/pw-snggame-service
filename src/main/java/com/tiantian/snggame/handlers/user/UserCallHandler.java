package com.tiantian.snggame.handlers.user;

import akka.actor.ActorRef;
import akka.actor.UntypedActorContext;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.sng.akka.event.user.TableUserCallEvent;
import com.tiantian.sng.akka.event.user.TableUserCheckEvent;
import com.tiantian.sng.akka.event.user.TableUserFoldEvent;
import com.tiantian.snggame.handlers.EventHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.*;
import com.tiantian.snggame.utils.GameUtils;
import com.tiantian.snggame.utils.RecordUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class UserCallHandler implements EventHandler<TableUserCallEvent> {
    @Override
    public void handler(TableUserCallEvent event, ActorRef self, UntypedActorContext context, ActorRef sender) {
        String userId = event.getUserId();
        String pwd = event.getPwd();
        String tableId = event.tableId();
        TableUser tableUser = TableUser.load(userId, event.tableId());
        if (tableUser.getSitNum() != null ) {
            TableStatus tableStatus = TableStatus.load(tableUser.getTableId());
            // 判断状态
            if (tableStatus != null && tableStatus.checkPwd(pwd)) {
                // 校验玩家是否能够跟注
                UserChips userChips = UserChips.load(userId, tableUser.getTableId());
                long maxBet = 0;
                try{
                    maxBet = Long.parseLong(tableStatus.getMaxBetSitNumChips());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // 计算已经下注额
                long userAlreadyBet = 0;
                // 设置call的值
                UserBetLog userBetLog = tableStatus.getUserBetLog(tableStatus.getCurrentBet());
                if (userBetLog != null) {
                    userAlreadyBet = userBetLog.getRoundChips();
                }
                long callChips = maxBet - userAlreadyBet;
                String[] ops = tableStatus.getUserCanOps(userId, tableStatus.getCurrentBet());
                String status = null;
                char op = ops[0].charAt(2);
                // 能不能跟注
                if ('0' == op) {
                    // 判断是否能够看牌
                    char checkOp = ops[0].charAt(1);
                    if ('0' == checkOp) {
                        status = GameEventType.FOLD;
                        Handlers.INSTANCE.execute(new TableUserFoldEvent(event.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    } else {
                        status = GameEventType.CHECK;
                        Handlers.INSTANCE.execute(new TableUserCheckEvent(event.tableId(), userId, pwd),
                                self, context, sender);
                        return;
                    }
                } else {
                    status = GameEventType.CALL;
                }
                // 增加玩家的操作计数
                tableUser.addOneOperateCount();
                tableUser.clearNotOperateCount();
                tableStatus.setSitBetStatusNotFlush(tableUser.getSitNum(), status);
                JSONObject object = new JSONObject();
                object.put("inner_id", tableStatus.getInningId());
                object.put("inner_cnt", tableStatus.getIndexCount());
                object.put("uid", userId);
                object.put("sn", Integer.parseInt(tableUser.getSitNum()));
                if (status.equalsIgnoreCase(GameEventType.CALL)) {
                    if (callChips <= 0) {
                        System.out.println("error: callChips is less or eq 0");
                        callChips = 0;
                    }
                    userChips.reduceAndFlushChips(callChips);
                    object.put("val", callChips);
                    // 玩家下注
                    tableStatus.userBet(tableUser.getSitNum(), callChips);
                }
                object.put("left_chips", userChips.getChips());
                object.put("bottom_pool", tableStatus.getTotalPoolMoney());
                tableUser.setBetStatus(status);
                tableUser.save();

                // 发送个桌子的玩家
                TableAllUser tableAllUser = TableAllUser.load(tableUser.getTableId());
                Collection<String> userIds = tableAllUser.getOnlineTableUserIds();
                String id = UUID.randomUUID().toString().replace("-", "");
                GameUtils.notifyUsers(object, status, userIds, id, tableId);

                // 通知玩家操作成功
                JSONObject object1 = new JSONObject();
                object1.put("inner_id", tableStatus.getInningId());
                object1.put("inner_cnt", tableStatus.getIndexCount());
                object1.put("op", status);
                GameUtils.notifyUser(object1, "call_suc", userId, tableId);

                GameRecord mttGameRecord = RecordUtils.getLastedRecord(tableStatus.getTableId());
                if (mttGameRecord == null) {
                    mttGameRecord = new GameRecord();
                }
                List<GameRecord.Progress> progresses = mttGameRecord.getProgresses();
                progresses.add(GameRecord.Progress.create("call", tableUser.getSitNum(), callChips + "",
                        System.currentTimeMillis() - mttGameRecord.getStartTime()));
                RecordUtils.restLastedRecord(mttGameRecord, tableId);

                // 判断游戏轮次
                GameStatus nextStatus = GameUtils.checkNextStatus(tableStatus, tableStatus.getStatus(), tableStatus.getMaxBetSitNum(),
                        tableUser.getSitNum());
                if (nextStatus != null) {
                    tableStatus.roundBetEnd();
                    boolean needShow = tableStatus.needShowAllCards();
                    if (needShow) { // 需要显示所有的手牌
                        List<Map<String, Object>> cardsList = tableStatus.userNotFoldCards();
                        JSONObject object2 = new JSONObject();
                        object2.put("inner_id", tableStatus.getInningId());
                        object2.put("inner_cnt", tableStatus.getIndexCount());
                        object2.put("all_cards", cardsList);
                        String id2 = UUID.randomUUID().toString().replace("-", "");
                        GameUtils.notifyUsers(object2, GameEventType.ALL_SHOW_CARDS, userIds, id2, tableId);
                    }
                    tableStatus.save();
                    // 通知玩家的池信息
                    GameUtils.notifyUserPoolInfo(tableStatus, tableAllUser);
                    // 触发下一轮的发牌事件
                    GameUtils.triggerNextStatusTask(nextStatus, tableUser.getTableId(), tableStatus.getInningId());
                    return;
                }

                //通知下一个玩家下注
                GameUtils.noticeNextUserBet(tableUser.getTableId(), tableStatus, null, false, null,
                        self, context, sender);
            }
        }
    }
}
