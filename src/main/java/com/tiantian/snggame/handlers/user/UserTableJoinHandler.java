package com.tiantian.snggame.handlers.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tiantian.redistask.proxy_client.RedisTaskIface;
import com.tiantian.sng.akka.event.TableTaskEvent;
import com.tiantian.sng.akka.event.task.RoomTableStartEvent;
import com.tiantian.sng.akka.event.user.TableUserJoinEvent;
import com.tiantian.snggame.akka.ClusterActorManager;
import com.tiantian.snggame.handlers.EventRetHandler;
import com.tiantian.snggame.handlers.Handlers;
import com.tiantian.snggame.handlers.helper.UserInfHandlerHelper;
import com.tiantian.snggame.manager.constants.GameConstants;
import com.tiantian.snggame.manager.constants.GameEventType;
import com.tiantian.snggame.manager.constants.GameStatus;
import com.tiantian.snggame.manager.model.TableAllUser;
import com.tiantian.snggame.manager.model.TableStatus;
import com.tiantian.snggame.manager.model.TableUser;
import com.tiantian.snggame.manager.model.UserChips;
import com.tiantian.snggame.utils.GameUtils;
import org.apache.thrift.TException;
import java.util.*;

/**
 *
 */
public class UserTableJoinHandler implements EventRetHandler<TableUserJoinEvent> {
    @Override
    public Object handler(TableUserJoinEvent event) {
        String userId = event.getUserId();
        String roomId = event.getRoomId();
        String tableId = event.tableId();
        int sitNum = event.getSitNum();
        int tableIndex = event.getTableIndex();
        String avatarUrl = event.getAvatarUrl();
        String nickName = event.getNickName();
        long buyIn = event.getBuyIn();
        int maxUsers = event.getMaxUsers();
        String gender = event.getGender();
        String roomName = event.getRoomName();

        List<TableUserJoinEvent.Rule> ruleList = event.getRuleList();
        List<TableUserJoinEvent.Reward> rewardList = event.getRewardList();
        TableUser tableUser = TableUser.load(userId, tableId);
        UserChips userChips = UserChips.load(userId, tableId);
        if (tableUser.isNull()) {
            tableUser = TableUser.init(userId, tableId);
            tableUser.setSitNum(sitNum + "");
            tableUser.setNickName(nickName);
            tableUser.setAvatarUrl(avatarUrl);
            tableUser.setBetStatus("");
            tableUser.setStatus("preparing");
            tableUser.setNotOperateCount("");
            tableUser.setOperateCount("");
            tableUser.setRoomId(roomId);
            tableUser.setGender(gender == null ? "0" : gender);
            tableUser.setTableIndex(tableIndex + "");
            tableUser.setTotalSecs(GameConstants.BET_DELAYER_TIME / 1000 + "");
            tableUser.save();
            if (userChips == null) {
                userChips = UserChips.init(userId, tableId, buyIn);
            }
        } else {
            tableUser.setStatus("gaming");
            tableUser.save();
        }
        TableStatus tableStatus = TableStatus.load(tableId);
        if (tableStatus == null)  {
            tableStatus = TableStatus.init(tableId);
            String taskId = UUID.randomUUID().toString().replace("-", "");
            tableStatus.setUserOnlineTaskId(taskId);
            tableStatus.setMaxUsers(maxUsers + "");
            tableStatus.setRoomId(roomId);
            tableStatus.setRoomName(roomName);
            tableStatus.setBuyIn(buyIn + "");
            tableStatus.setSmallBlindMoney(ruleList.get(0).getSmallBlind() + "");
            tableStatus.setBigBlindMoney(ruleList.get(0).getBigBlind() + "");
            tableStatus.setCurrentBlindLevel(0 + "");
            tableStatus.setBlindRules(JSON.toJSONString(ruleList));
            tableStatus.setRewards(JSON.toJSONString(rewardList));
            tableStatus.setStatus("");
            tableStatus.save();
        }
        TableAllUser tableAllUser = TableAllUser.load(tableId);
        tableAllUser.joinUser(userId, sitNum + "");

        String innerId = tableStatus.getInningId() == null ? "" : tableStatus.getInningId();
        String indexCount = tableStatus.getIndexCount() == null ? "0" : tableStatus.getIndexCount();
        JSONObject object = new JSONObject();
        object.put("inner_id", innerId);
        object.put("inner_cnt", indexCount);
        object.put("user_id", userId);
        object.put("room_id", "sng_room");
        object.put("table_id", tableId);
        object.put("gender", tableUser.getGender() == null ? "0" : tableUser.getGender());
        object.put("sit_num", Integer.parseInt(tableUser.getSitNum()));
        object.put("avatar_url", tableUser.getAvatarUrl() == null ? "" : tableUser.getAvatarUrl());
        object.put("nick_name", tableUser.getNickName());
        object.put("money", userChips.getChips());
        object.put("max_people", maxUsers);
        object.put("sb", Long.parseLong(tableStatus.getSmallBlindMoney()));
        object.put("bb", Long.parseLong(tableStatus.getBigBlindMoney()));
        String id = UUID.randomUUID().toString().replace("-", "");
        List<String> toUsers = new ArrayList<>();
        TableAllUser reloadTableUser = TableAllUser.load(tableId);
        object.put("wait_nums", Integer.parseInt(tableStatus.getMaxUsers()) - reloadTableUser.getJoinTableUserMap().size());
        if (reloadTableUser.getOnlineTableUserIds() != null) {
            Collection<String> tableUsers = reloadTableUser.getOnlineTableUserIds();
            if (tableUsers.size() > 0) {
                for (String uId : tableUsers) {
                    // 不发给自己
                    if (uId.equals(userId)) {
                        continue;
                    }
                    toUsers.add(uId);
                }
            }
            if (toUsers.size() > 0) {
                // 发送加入游戏消息给其他玩家
                GameUtils.notifyUsers(object, GameEventType.JOIN_ROOM, toUsers, id, tableId);
            }
        }
        UserInfHandlerHelper.sendUserInfos(tableStatus, reloadTableUser, tableId, userId, nickName, avatarUrl, gender);
        //判断总人数
        if (!tableStatus.isStarted()) { // 游戏没有开始
            int sitSize = reloadTableUser.getJoinTableUserMap().size();
            if (sitSize == maxUsers) {
                tableStatus.setStatus(GameStatus.READY.name());
                //开始游戏
                tableStatus.setLastUpBlindTimes(System.currentTimeMillis() + "");
                tableStatus.saveNotAddCnt();

                for (String uId : reloadTableUser.getJoinTableUserMap().values())  {
                     TableUser tbUser = TableUser.load(uId, tableId);
                     tbUser.setStatus("gaming");
                     tbUser.save();
                }

                // 随机庄家位
                Random random = new Random();
                int randomIndex = random.nextInt(maxUsers);
                //延迟10s开始游戏
                delayStart(tableId, (randomIndex + 1) + "");
                // 开始
                ClusterActorManager.tellRoom(new RoomTableStartEvent(roomId, tableId));
            }
        }
        return "ok";
    }

    private void delayStart(String tableId, String randomBtn) {
        JSONObject data =  new JSONObject();
        // 发送一个延迟3s的开始, 确定庄家大小盲注任务
        data.put(GameConstants.TASK_EVENT, GameStatus.BEGIN.name());
        data.put("tableId", tableId);
        data.put("randomBtn", randomBtn);
        try {
            RedisTaskIface.instance().iface().pushTask(data.toJSONString(), GameConstants.BEGIN_DELAYER_TIME_SECS);
        } catch (TException e) {
            e.printStackTrace();
        }
    }
}
