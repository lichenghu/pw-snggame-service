package com.tiantian.snggame.handlers;


/**
 *
 */
public interface EventRetHandler <T>{
    Object handler(T event);
}
