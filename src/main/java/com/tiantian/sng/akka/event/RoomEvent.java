package com.tiantian.sng.akka.event;

/**
 *
 */
public interface RoomEvent extends Event {
    String roomId();
}
