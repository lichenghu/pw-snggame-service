package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserExitEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private boolean isTrueExit;
    public TableUserExitEvent(String tableId, String userId, boolean isTrueExit) {
        super(true);
        this.tableId = tableId;
        this.userId = userId;
        this.isTrueExit = isTrueExit;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userExit";
    }

    public String getUserId() {
        return userId;
    }

    public boolean getIsTrueExit() {
        return isTrueExit;
    }
}
