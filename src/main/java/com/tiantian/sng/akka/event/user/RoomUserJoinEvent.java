package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.RoomEvent;

import java.io.Serializable;

/**
 *
 */
public class RoomUserJoinEvent implements RoomEvent {

    private String roomId;
    private String clubId;
    private String userId;
    private String avatarUrl;
    private String userName;
    private int maxTableNums;
    private int maxUserNums;
    private long fee;

    public RoomUserJoinEvent(String roomId, String clubId, String userId, String avatarUrl, String userName,int maxTableNums, int maxUserNums, long fee) {
        this.roomId = roomId;
        this.clubId = clubId;
        this.userId = userId;
        this.avatarUrl = avatarUrl;
        this.userName = userName;
        this.maxTableNums = maxTableNums;
        this.maxUserNums = maxUserNums;
        this.fee = fee;
    }

    @Override
    public String roomId() {
        return roomId;
    }

    @Override
    public String event() {
        return "joinRoom";
    }

    public String getUserId() {
        return userId;
    }

    public int getMaxTableNums() {
        return maxTableNums;
    }

    public int getMaxUserNums() {
        return maxUserNums;
    }

    public String getClubId() {
        return clubId;
    }

    public long getFee() {
        return fee;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getUserName() {
        return userName;
    }

    public static class Response implements Serializable {
        private int status;
        private String tableId;
        private int sitNum;
        private int tableIndex;

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getTableId() {
            return tableId;
        }

        public void setTableId(String tableId) {
            this.tableId = tableId;
        }

        public int getSitNum() {
            return sitNum;
        }

        public void setSitNum(int sitNum) {
            this.sitNum = sitNum;
        }

        public int getTableIndex() {
            return tableIndex;
        }

        public void setTableIndex(int tableIndex) {
            this.tableIndex = tableIndex;
        }
    }
}
