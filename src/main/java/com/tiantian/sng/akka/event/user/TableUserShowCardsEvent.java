package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserShowCardsEvent extends TableUserEvent {
    private String tableId;
    private String userId;
    private String data;

    public TableUserShowCardsEvent(String tableId, String userId, String data) {
        this.tableId = tableId;
        this.userId = userId;
        this.data = data;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "showCards";
    }

    public String getData() {
        return data;
    }
}
