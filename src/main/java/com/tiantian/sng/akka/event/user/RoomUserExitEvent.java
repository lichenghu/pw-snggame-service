package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.RoomEvent;

/**
 *
 */
public class RoomUserExitEvent implements RoomEvent {
    private String roomId;
    private String userId;
    private String tableId;

    public RoomUserExitEvent(String roomId, String userId, String tableId) {
        this.roomId = roomId;
        this.userId = userId;
        this.tableId = tableId;
    }

    @Override
    public String roomId() {
        return roomId;
    }

    @Override
    public String event() {
        return "exitRoom";
    }

    public String getUserId() {
        return userId;
    }

    public String getTableId() {
        return tableId;
    }
}
