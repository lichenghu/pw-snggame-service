package com.tiantian.sng.akka.event.user;

import com.tiantian.sng.akka.event.TableUserEvent;

/**
 *
 */
public class TableUserRejoinEvent extends TableUserEvent {
    private String tableId;
    private String userId;

    public TableUserRejoinEvent(String tableId, String userId) {
        this.tableId = tableId;
        this.userId = userId;
    }

    @Override
    public String tableId() {
        return tableId;
    }

    @Override
    public String event() {
        return "userRejoin";
    }

    public String getUserId() {
        return userId;
    }
}
