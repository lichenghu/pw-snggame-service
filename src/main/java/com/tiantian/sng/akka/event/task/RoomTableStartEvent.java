package com.tiantian.sng.akka.event.task;

import com.tiantian.sng.akka.event.RoomEvent;

/**
 *
 */
public class RoomTableStartEvent implements RoomEvent {
    private String roomId;
    private String tableId;

    public RoomTableStartEvent(String roomId, String tableId) {
        this.roomId = roomId;
        this.tableId = tableId;
    }

    @Override
    public String roomId() {
        return roomId;
    }

    @Override
    public String event() {
        return "room_table_start";
    }

    public String getTableId() {
        return tableId;
    }
}
